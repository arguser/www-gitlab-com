<!--

Describe the change and rationale here.

-->

### Approvals

Merge requests with changes to stages and groups and significant 
changes to categories need to be created, approved, and/or merged 
by each of the below:

- [ ] EVP of Product
- [ ] VP of Product Strategy
- [ ] The Product Director relevant to the stage group(s)
- [ ] The Engineering Director relevant to the stage group(s)
- [ ] CEO

The following people need to be on the merge request so they stay informed:

- [ ] EVP of Engineering `@edjdev` <!--  Only required for significant changes -->
- [ ] Senior Director of Development `@clefelhocz1`
- [ ] Director of Quality `@meks`
- [ ] The Product Marketing Manager relevant to the stage group(s)

<!--
Changes that require executive approval include:
- Changes to a stage, group, or category name
- Removal or addition of a stage, group, or category

Changes that require approval only from the relevant Product Director include:
- Changing a category maturity date
- Changes to section or group member lists
- Changes to a category vision page

More information can be found in the Category Change section: 
https://about.gitlab.com/handbook/product/categories/#changes

-->
