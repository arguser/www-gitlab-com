---
layout: job_family_page
title: "Technical Writing Management"
---

### Technical Writing Management

Technical Writing Managers in the UX department at GitLab see the team as their product. While they are credible as Technical Writers and know the details of what Technical Writers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of technical writing commitments and are always looking to improve productivity and process. They must also coordinate across departments to accomplish collaborative goals.

### Technical Writing Manager

The Technical Writing Manager reports to the Senior Manager, Technical Writing and a team of Technical Writers report to the Technical Writing Manager.

## Responsibilities

* Manage a Technical Writing team.
* Maintain a single source of truth across our sites, documentation, and tutorials.
* Deliver input on promotions, function changes, demotions, and offboarding in consultation with the other Technical Writing Managers, the UX Director, and the Senior Manager, Technical Writing. 

### Documentation

* Manage documentation improvement efforts.
* Create practices that encourage keeping documentation up to date and easily discoverable.
* Make sure by the time of release all documentation, guides, and related content were shipped.
* It is not the responsibility of the Technical Writing team to write the first
documentation draft for new or updated features.
* Create tutorials on a workflow designed around GitLab's defining features.

### Website

* Work with the Sales and Marketing team leads to make sure the website product content is comprehensive and up to date.
* Create a self-managing framework for the Community Writers program.
* Grow the Remote Only and Conversational Development websites.

## Ongoing Projects

* Reorganize documentation around topics, grouping related content into topic
  portals with a topic index page to overview the included content, taking
  readers on journey from getting started as a beginner to advanced administration.
* Create marketing website pages comparing GitLab features vs popular competing products.
* Separate documentation rendering from the rails app. Either update GitLab's
  `/help` to point to `docs.gitlab.com` or local build of the static site documentation.
* Work with the VP of Product to empower PMs and Engineers to write the first
  draft of documentation for new or updated features. Use the release blog
  post as a changelog checklist to ensure everything is documented.

## Requirements

* Experience in setting up online documentation is a hard requirement for this role.
* Previous experience in using Git is a hard requirement for this role.
* Proven experience in managing, mentoring, and training teams of Technical Writers
* Experience working on production-level documentation
* Self-motivated and strong organizational skills
* Strong written and spoken communication skills
* Familiarity with Static Site Generators, HTML, CSS
* Familiarity with Continuous Integration, Containers, Kubernetes and Project Management software a plus
* Be able to work with the rest of the community
* You share our values, and work in accordance with those values
* [Adhere to our view of Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

## Hiring Process 

* Screening call with a recruiter.
* Interview with Technical Writer. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, as well as what type of teams you have led and your management style. The interviewer will also be looking to understand how you define strategy, how you work with cross functional partners, how you've handled conflict, and how you dealt with difficult situations in the past. Do be prepared to talk about your approach to writing work, experience with docs-as-code workflows, and your technical abilities, too.
* Interview with Technical Writing Manager. In this interview, we want you to share a case study presentation that provides insight to a project you led. We'll look to understand the size and structure of your team, the goals of the project, the tool chain you 
used, how you managed the team and workload, and how you collaborated with cross-functional partners. Broadly, we want to hear how you identified what needed to be done and then guided your team to the end result.
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led, and your management style. The interviewer will also want to understand how you define strategy, how you work with cross-functional partners, how you've handled conflict, and how you've dealt with difficult situations in the past. Do be prepared to talk about your work and technical abilities, too.

### Senior Manager, Technical Writing

The Senior Manager, Technical Writing reports to the UX Director. A team of Technical Writing Managers report to the Senior Manager, Technical Writing.

## Responsibilities

* Manage a team of Technical Writing Managers.
* Ensure teams maintain a single source of truth across our sites, documentation and tutorials.
* Coordinate regular content audits for documentation in your area of responsibility.
* Help improve the usability of docs.gitlab.com.
* Deliver input on promotions, function changes, demotions and offboarding in consultation with the Technical Writing Managers and the UX Director. 
* Hire a world-class team of Technical Writing Managers and Technical Writers to work on their teams
* Help their managers and writers grow their skills and experience.
* Drive quarterly [Technical Writing OKRs](https://about.gitlab.com/company/okrs/).

## Requirements

* Experience in setting up online documentation.
* Previous experience in using command-line Git.
* Proven experience in managing, mentoring, and training teams of Technical Writers and Technical Writing Managers.
* Experience working on production-level documentation.
* Self-motivated and strong organizational skills.
* Strong written and spoken communication skills.
* Familiarity with Static Site Generators, HTML, CSS.
* Familiarity with Continuous Integration, Containers, Kubernetes, and Project Management software a plus.
* Be able to work with the rest of the community.
* Experience collaborating with cross-functional partners at all levels of seniority, from junior IC to VP. 
* Ability to shape documentation process in a high-growth, ever-changing organisation.
* Give clear, timely, and actionable feedback.
* Experience improving scheduling processes to balance documentation work.
* You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
* [You adhere to our view of Leadership at GitLab.](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab.

## Performance indicators

* [Hiring Actual vs Plan](/handbook/engineering/ux/performance-indicators/#hiring-actual-vs-plan)
* [Diversity](/handbook/engineering/ux/performance-indicators/#diversity)
* [Handbook Update Frequency](/handbook/engineering/ux/performance-indicators/#handbook-update-frequency)
* [Team Member Retention](/handbook/engineering/ux/performance-indicators/#team-member-retention)

## Hiring Process 

* Screening call with a recruiter.
* Interview with Technical Writer. In this interview, the interviewer will spend time trying to understand the experiences and challenges you've had as a manager, along with the documentation-related tooling you are comfortable with and how you've addressed various content usability considerations.
* Interview with Technical Writing Manager. In this interview, we want to get to know how you think about the place of the tech writing team within an organization, ways of ensuring the team is well-positioned to produce content that has a strong impact on the user experience, and specific examples of large content initiatives. 
* Interview with UX Director. In this interview, the interviewer will spend a lot of time trying to understand the experience you have as a manager, what types of teams you have led and your management style. The interviewer will also want to understand how you define strategy, your approach to content usability and improvement, how you've handled difficult situations, and how you've helped mentor technical writers. Do be prepared to talk about your work and technical abilities, too.
* Interview with a VP of Engineering. In this interview, we'll look to understand your leadership experience, management style, and what is driving you to want to join GitLab. 
