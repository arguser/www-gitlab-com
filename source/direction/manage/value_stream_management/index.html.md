---
layout: markdown_page
title: "Category Direction - Value Stream Management"
---

- TOC
{:toc}

| Category | **Value Stream Management** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) |
| Group | [Analytics](https://about.gitlab.com/handbook/product/categories/#analytics-group) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-02-12` |

### Introduction and how you can help
Thanks for visiting the direction page for Value Stream Management in GitLab. This page is being actively maintained by the Product Manager for the [Analytics group](https://about.gitlab.com/handbook/product/categories/#access-group). If you'd like to contribute or provide feedback on our category direction, you can:

1. Comment and ask questions regarding this category vision by commenting in the [public epic for this category](https://gitlab.com/groups/gitlab-org/-/epics/215).
1. Find issues in this category accepting merge requests. 

### Overview

![Analytics Overview](/images/direction/dev/vsm-framework.png)

Over the years, software has become more powerful and specialized; however, the teams who create, build, deliver, and analyze the performance of products are more disconnected, despite the increasing time spent on updates and status reports. In order to scale and speed up the delivery of quality products, silos have to be broken but the business, product managers and engineers still use different tools glued together by imperfect integrations. We strongly believe that having a single application for the entire software development lifecycle is already a huge step forward, but we are only just starting to explore ways to surface valuable insights and recommendations, which will help organizations increase transparency and productivity across teams and connect the business with engineering.

[Value Stream Mapping in Software Development](https://en.wikipedia.org/wiki/Value-stream_mapping) focuses on understanding and measuring the value added by the flow of activities in the software development lifecycle. In the time of technological disruption we are in, success will be largely dependent on the ability of enterprises to define, connect and manage software and business value streams. Often times, this will coincide with a culture shift requiring many enterprises to adapt the way they work. At GitLab, we are making a small step towards connecting the business with engineering by using issues and MRs with [labels](https://gitlab.com/help/user/project/labels.md#scoped-labels) associated with OKRs, for example.

Our first attempt at helping organizations get a better understanding of their flow was the introduction of [Cycle Analytics](https://docs.gitlab.com/ee/user/project/cycle_analytics.html) for all customers that follow the [GitLab Flow](https://about.gitlab.com/solutions/gitlab-flow/). Cycle Analytics got a lot of attention and we quickly understood that while we are striving to define best practices, different organizations have different value streams and workflows and we need to support the ability to define and measure these customized workflows in GitLab in order to move the industry forward and serve our customers better. As of June 2019, Cycle Analytics is officially part of our [VSM solution](https://about.gitlab.com/solutions/value-stream-management/).

### Where we are Headed
We are building GitLab with the goal of having teams manage their entire development life cycle and we believe that an integrated solution will enable teams to be faster and more efficient. However, you cannot measure what you cannot see, so we want to make it easy for Engineering Leadership to quickly get a unified view of the end-to-end flow of how software value is created by their teams. We believe we can capture the vast majority of the processes metadata within GitLab since we already have tools for ideation, planning, design, development, testing, code review, security testing and release.

#### Identifying and Drilling Down into Sources of Waste
By providing customers with the ability to define their processes at different levels of granularity and to calculate the time it takes to complete each stage of the process, users should be able to quickly spot where their relative bottlenecks are. However, in order to get to the bottom of a problem, we need to allow users to drill-down. This is why we will build a catalog of configurable chart widgets, which users could quickly add to their dashboard to gain quick insights of possible causes of slow-moving stages in their process.

In general, through value stream mapping, we hope to be able to show you the steps where you can improve your practices by segregating the value added activities from those that contribute to waste. We are going to get to a stage where we compel customers to ask the important questions of how much value each activity adds compared to the time and resources it takes. By visualizing hand-offs identified from our unified data model, it should become clear where teams can improve.

#### Recommendations
Through our product, we have worked with a vast host of engineering teams, which has helped us identify common patterns of success and failure in DevOps. We are planning to encode this knowledge and given the data we enable users to store in their instance, we would like to automatically highlight process bottlenecks and generate recommendations on how to minimize their impact. We would also like to enable customers to set targets for the different stages of their process, which we can follow up on with reminders and suggestions. 

Recommendations will be ranked in order of importance by the value they bring in reducing time to production and increasing quality. Through your feedback on the quality of those recommendations and industry developments, we will continue to improve our algorithm to provide more tailored suggestions.

#### Faster Time to Production and Increased Quality
It's important that data and performance is evaluated with a focus on outcomes. Whether we succeed in enabling customers to visualize the bottlenecks in their process can be objectively measured by faster time to production. According to the 2018 DORA report, Elite DevOps performers take less than an hour to get from code committed to production, while low performers - anywhere between one and six months. We will show you trends of how well you are doing over time plotted against industry benchmarks or your own organizational targets. Being fast, however should not come at the cost of wasteful coding practices creating a lot of quality and instability issues. High and elite performers in DevOps show high correlation between faster time to production and rare, quickly identified and resolved issues. We will provide KPIs coming from our monitoring stage such as MTTD, MTTR to provide the full picture of the health of your value stream.

### Target Audience and Experience
During the minimal and viable states of the category, we aim to cater to the needs of [Development Team Leads](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead) (primarily) and [Product Managers](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#parker-product-manager) (secondarily) in their efforts to improve velocity and predictability of their value streams. As we build out the category, Department Leads and Senior Executives will have one place where they can oversee the progress of their teams and identify best practices and negative pattens that inspire improvements across their organizations.

### Maturity Plan
This category is currently **Minimal**. The next step in our maturity plan is achieving a **Viable** state of maturity.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

For VSM to be Viable, we must have a way of measuring time across the full the value stream - it should be a sum of each component of value stream analytics. It should give users something valuable out of the box without running into data errors.
* You can track our journey toward this level of maturity in the [Viable maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1478) for this category.

### Competitive Landscape

#### Tasktop
[TaskTop](https://www.tasktop.com/integration-hub) is exclusively focused on Value Stream Management and allows users to connect more than 50 tools together, including Atlassian's JIRA, GitLab, GitHub, JamaSoftware, CollabNet VersionOne, Xebia Labs, and TargetProcess to name a few. Tasktop serves as an integration layer on top of all the software development tools that a team uses and allows for mapping of processes and people in order to achieve a common data model across the toolchain. End users can visualize the flows between the different tools and the data can be exported to a database for visualization through BI tools.

While we understand that not all users of GitLab utilize all of our stages, we believe that there is already a lot of information across planing, source code management and continuous integration and deployment, which can be used to deliver valuable insights.

We are starting to build dashboards, which can help end users visualize a custom-defined value stream flow at a high level and drill down and filter to a specific line of code or MR.

#### CollabNet VersionOne
[CollabNet VersionOne](https://www.collab.net) provides users with the ability to input a lot of information, which is a double edged sword as it can lead to duplication of effort and stale information when feeds are not automated. It does however, allow a company to visualize project streams from a top level with all their dependencies. End users can also create customizable reports and dashboards that can be shared with senior management.

#### Plutora
[Plutora Analytics](https://www.plutora.com/platform/plutora-analytics) seem to target mainly the release managers with their [Time to Value Dashboard](https://www.plutora.com/platform/time-to-value-dashboard). The company also integrates with JIRA, Jenkins, GitLab, CollabNet VersionOne, etc but there is still a lot of configuration that seems to be left to the user.

#### TargetProcess
[Targetprocess](https://www.targetprocess.com) tries to provide full overview over the delivery process and integrates with Jenkins, GitHub and JIRA. The company also provides customizable dashboards that can give an overview over the process from ideation to delivery.

#### GitPrime
Although [GitPrime](https://www.gitprime.com) doesn't try to provide a value stream management solution, it focuses on productivity metrics and cycle time by looking at the productivity of a team. It exclusively uses only git data.

#### Azure DevOps
Naturally, [Azure](https://docs.microsoft.com/en-us/azure/devops/report/dashboards/analytics-widgets?view=azure-devops) is working on adding analytics that can help engineering teams become more effective but it's still in very early stages. It has also recently acquired [PullPanda](https://pullpanda.com).

#### Velocity by Code Climate
Similarly to GitPrime, [Code Climate](https://codeclimate.com/velocity/understand-diagnose/) focuses on the team and uses git data only.

#### Gitalytics
Similarly to GitPrime, [Gitalytics](https://gitalytics.com) focuses on the team and uses git data only.

#### Gitential
[Gitential](https://gitential.com)

#### Gitclear
[Gitclear](https://gitclear.com)

#### Gitlean
[Gitlean](https://gitlean.com)

#### XebiaLabs
[XebiaLabs' analytics](https://docs.xebialabs.com/xl-release/concept/release-dashboard-tiles.html) are predominantly focused on the Release Manager and give useful overviews of deployments, issue throughput and stages. The company integrates with JIRA, Jenkins, etc and end users can see in which stage of the release process they are.

#### CloudBees
[CloudBees DevOptics](https://www.cloudbees.com/products/cloudbees-devoptics) is focused on giving visibility and insights to measure, manage and optimize the software delivery value stream. It allows comparisons across teams and integrates with Jenkins and Jira and SVM /VCS solutions.

#### CA Technologies
[CA Agile Central](https://docs.ca.com/en-us/ca-agile-central/saas/iteration-burndown) combines data across the planning process in a single integrated page with custom applications available to CA Agile Central users. The applications can be installed in custom pages within CA Agile Central or on a dashboard.

#### Atlassian's JIRA Align
[Agile Craft](https://agilecraft.com)

#### PivotalTracker
[PivotalTracker](https://www.pivotaltracker.com/help/articles/analytics_charts_and_reports_overview/)

### Kanbanize
[Kanbanize](https://kanbanize.com/dashboards-analytics/)

### Analyst Landscape
Forrester's New Wave: Value Stream Management Tools, Q3 2018 uncovered an emerging market with no leaders. However, vendors from different niches of the development pipeline are converging to value stream management in response to customers seeking greater transparency into their processes.

Forrester’s vision for VSM includes:
- end-to-end visibility of the software development process, including the corresponding capture and storage of data, events, and artifacts
- definition and visualization of key performance indicators (KPIs)
- inclusive customer experience, which allows multiple roles (PMs, developers, QA, and release managers) to collaborate in one place
- governance, i.e. a framework to monitor compliance to organizational standards, automated audit capabilities and traceability.

Additional functionality, requested by clients includes:
- integration with other tools, including the ability to double-click into each tool to directly observe status or take action
- business value custom definitions in terms of financials, time, effort or similar
- mapping of business value, people, processes, data
- visualization dashboards, which users can customize to support different role-based views.

