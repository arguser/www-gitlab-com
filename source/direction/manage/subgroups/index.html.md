---
layout: markdown_page
title: "Category Direction - Subgroups"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [groups](https://gitlab.com/groups/gitlab-org/-/epics?label_name=groups) |

## Introduction and how you can help
Thanks for visiting this category page on Subgroups within GitLab. This page belongs to the Spaces group of the Manage stage, and is maintained by [Luca Williams](https://gitlab.com/tipyn) who can be contacted directly via [email](mailto:luca@gitlab.com). You can also [open an issue](https://gitlab.com/gitlab-org/gitlab/issues) and @mention them there. Please add the `group::spaces` label for wider visibility.

This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute to this vision. If you’re a GitLab user and would like to talk to us about any of the topics covered below, we’d especially love to hear from you. 

## Groups

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/#prefer-small-primitives)) in GitLab for project organization and managing access to these resources at scale. Like folders in a filetree, they organize like resources into one place - whether that’s a handful of projects all belonging to a backend team, or organizing a group of user researchers into one place to make access control and communication easier.

Since groups cover multiple projects, we also scope a number of features at the group level like  epics, contribution analytics, and the security dashboard. 

In 2020, our goal is to improve groups with improvements to access control, enhancements to the user experience, and by extending subgroups to be able to meet two important use cases: one to help Enterprise organizations thrive on GitLab.com, and one to support teams of people. 

### Isolation 

#### Problem

With the proliferation of Cloud services and more companies taking advantage of managed infrastructure, SaaS products need to work well for large customers. Historically, Enterprise customers have gravitated toward Self-Managed GitLab as their favored solution. Now that we've grown with our customer base, we need to focus on the importance of providing a SaaS platform which can not only safely and securely house our customers and users, but also deliver a rich and fulfilling Enterprise experience that looks and feels exactly like their own private instance of GitLab whilst simultaneously reducing the load, cost and cycle time of having to manage and configure their own infrastructure.

We seek to solve a number of problems with the current SaaS experience:
* **Isolation** - Group owners on GitLab.com may unintentionally expose users, projects, or other sensitive information to the rest of the GitLab.com instance.
* **Control** - Since users on GitLab.com are attributed to the instance - not the group - group owners lack the ability to directly administer, manage and assist users in the group, instead working through GitLab Support. 

#### Where We're Headed

To solve the isolation challenge, we introduced the concept of [group managed accounts](https://gitlab.com/groups/gitlab-org/-/epics/709) in 12.1. This is a useful tool that creates a "group-level user", which requires that a person use a particular user account to interact with a specific group. This is very useful for organisations who want to require a separate account for work in the group - this is a typical use case for GitLab.com, where a top-level group may represent a company. Group Managed Accounts currently requires SSO/SAML to be configured. 

Group Managed Accounts are a fantastic start towards improving Enterprise isolation, security and privacy. In order to better accommodate Enterprise customers, we need to build an additional, secure layer around Groups, Projects and the teams of people working in them in order to give them the space they need to spread out, work, create and communicate with each other and Group Managed Accounts is the first step to do that. The next step is for us to work on extending [Group Managed Accounts](https://docs.gitlab.com/ee/user/group/saml_sso/#group-managed-accounts) and iterate from there.

We are essentially moving towards a new, top-level isolated type of group which allows multiple parent groups within it. Eventually, we hope to take this concept beyond solving for isolation and better administration - it can offer a way to bring instance-level features to the top-level group and introduce a collaboration platform across many groups and projects. 

<!-- To add later - [sketches of "what this could be" vision] -->

Some ideas of what this could look like includes features such as group-level activity feeds; Instance-level (top-level group) epics and issues; Full and autonomous user management and unified access control; Strong privacy features and autonomous data protection; Productivity highlights and hotspots in your organisation; Configurable and informative dashboards; A blog platform; Scheduling; Integrated chat functionality and more. The goal is to move even further towards a single application by enriching the user experience in such a way that you never need to leave GitLab to get the job done.

## Target audience and experience

Groups are used by all GitLab users, no matter what role. However, heavier users of Subgroups in terms of organisation and management of projects and users consist of:

* **Group Owners**
* **System Admins**
* **Team Leaders, Directors, Managers**
* **Individual contributors** who predominantly work with GitLab's project management features

## What's Next and Why

#### Do More With Group Managed Accounts

We introduced the first iteration of [Group Managed Accounts](https://docs.gitlab.com/ee/user/group/saml_sso/#group-managed-accounts) as a concept specific to SAML SSO. Our next important step is to extend this "group-level user" idea to other groups without requiring the use of this authentication strategy. We will do this by introducing a cleaner, more elegant way for Group Owners to easily create and manage users at the parent group level without the need for SSO/SAML enforcement, and also provide a clear login experience for those group-managed users.

You can follow along with the progress of this effort [here](https://gitlab.com/groups/gitlab-org/-/epics/2777). We aim to deliver this MVC by 12.10 and will be focusing on only this work for that milestone.

## Maturity

As Subgroups are a GitLab-specific concept, it's considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

<!-- ## Top user issue(s) -->

## Top internal customer issue(s)

🚨 GitLab Support Engineering: [Isolation & Control Requirements](https://gitlab.com/groups/gitlab-org/-/epics/2444)

<!-- ## Top Vision Item(s)

TBD -->
