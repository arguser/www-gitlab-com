---
layout: markdown_page
title: "Product Vision - Configure"
---

- TOC
{:toc}

The Configure stage solves user problems related to the configuration and operation of applications
and infrastructure. At the Configure stage within GitLab, we aim to make developers and operators more productive while
conforming to their platform/technology of choice and providing integrated workflows. We 
want to enhance these tools by providing integrated workflows and views inside GitLab. 

Our vision is that DevOps teams will use GitLab as their primary day-to-day tool as it will provide first-class operator support.

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Aconfigure).

## Overview

The Configure stage solves user problems related to the configuration and operation of applications
and infrastructure, including features like Auto DevOps, Kubernetes integration, runbooks, serverless workloads, and ChatOps.
We aim to make complex tasks (such as standing up new environments) fast and
easy. We provide operators all the necessary tools to execute their
day-to-day actions upon their infrastructure.

### Target personas

We build solutions targeting the following personas:

* [Software Developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [DevOps Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Systems Administrator](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)​

On small teams, these personas may be just one individual, while on larger teams they may be dedicated roles.

In keeping with our [single application](https://about.gitlab.com/handbook/product/single-application/) promise, we want
GitLab to be a robust, best-of-breed tool for operators as much as it is for developers. Our vision is that operators
will use GitLab as their main day-to-day tool for provisioning, configuring, testing, and decomissioning infrastructure.

### Strategic directions

We build products primarily for an 8 team SaaS product company, likely with a serverless or docker based architecture. 
In the latter case, we assume it's deployed either to Kubernetes or with Terraform to anything that Terraform supports.

### Challenges

We understand GitLab will play side-by-side with existing tools which teams have already invested considerable time and 
money. As a result, we will extend GitLab's Configure features to provide an outstanding, integrated experience with the 
preferred tools in the industry. Keeping up with the quickly changing nature of these tools is our primary challenge.

The technologies where we want to provide an outstanding, integrated experience with are 

- Kubernetes deployment, management, and monitoring
- Terraform usage, planning, and application
- Configuration management behind Terraform scripts

### Opportunities

Our opportunities in the Configure stage are derived from the [Ops Section opportunities](/direction/ops/#opportunities). 
Namely:

* **IT operations skills gap:** Operations skills are difficult to attract. We'll make it easier for you to leverage your 
operations expertise by efficiently managing your infrastructure platform. 
* **Clear winner in Kubernetes:** Our focus on enabling cloud-native applications based on the winner in container 
orchestration and management, Kubernetes, means you can leverage the broader Cloud Native ecosystem as you adopt modern 
application architectures.

This leads us to look into common workflows that we would like to support with a market-leading experience:

1. Flexible and robust Kubernetes cluster management
1. Infrastructure as Code, Policy as Code
1. Chaos Engineering
1. Serverless
1. Runbooks
1. ChatOps
1. GitOps

## North Stars

We're working on increasing the number CI/CD pipelines going through our infrastructure management features
(either K8s, Auto DevOps, Serverless or IaC in general, ChatOps).

We aim to achieve these by focusing on the following principles.

### Easily run your application on any infrastructure

We recognize getting started with modern devops practices from zero is hard. With Auto DevOps, GitLab already
provides a great framework to automatically detect, build, test, secure, deploy, and monitor your application.
We'll extend Auto DevOps to provide further customization of your application, such as automatically securing
it with an SSL certificate or easily define a custom domain. Additionally, we will extend its deployment capabilities
to provide built-in best practices, such as blue/green deployments, postgress replication, and postgres backup/restore.

If your application is not a good candidate for Auto DevOps, GitLab will provide great templates with smart defaults
which will provide a great experience for other approaches (such as serverless and micro-services) making tasks such as
scaling functions and inter-connecting micro-services, quick & easy.

### Detect and track automatically

Teams who begin their journey with GitLab may already have existing Kubernetes resources in place. So these can be
used effectively, we will detect and track the state of applications which are already installed in-cluster.
Following sensible conventions, once a cluster is added to any level of your workflow, GitLab will show which
apps are installed and offer all relevant features, regardless of how those apps have been installed.

### Provision everywhere

Provisioning infrastructure is a critical responsibility of the operations team. GitLab will make it easy to provision
resources either directly with your cloud of choice or by leveraging your existing infrastructure-as-code resources. To
that end, GitLab will offer cluster creation workflows with every major cloud provider as well as robust integration
with leading IaC providers such as Terraform.

For those teams just getting started with IaC, GitLab will lower the intial investment to use Terraform, with Git
as the source of truth for IaC.

### Resiliency for everyone

Providing a resilient production environment is an important part of day-to-day Ops. However, planning for real-world
outages, edge conditions, and erratic applications behavior, is not always possible. GitLab will provide seamless
integration with cloud-native chaos engineering tools, allowing ops teams to easily and realistically run
experiments on their infrastructure. Insights gained from these experiments will allow Ops teams to harden their
deployments and plan for real-world failures.

### First-class operator support

We want operators to use GitLab as their main day-to-day system. In addition to managing infrastructure, GitLab will
provide features for downtime action planning (via Runbooks) as well as ways to quickly execute actions on both
applications and infrastructure (via ChatOps).

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Configure stage is the **Configure 
SMAU** ([stage monthly active users]()). 

Configure SMAU is determined by tracking how users *configure*, *interact*, and *view* the features contained within the 
stage. The following features are considered:

| Configure                                                                                                                               | Interact                         | View                                                                 |
|-----------------------------------------------------------------------------------------------------------------------------------------|----------------------------------|----------------------------------------------------------------------|
| Create Kubenetes cluster on GKE                                                                                                         | Dismiss Auto DevOps banner       | View cluster health metrics                                          |
| Add existing Kubernetes cluster                                                                                                         | Run Auto DevOps pipeline         | View serverless page                                                 |
| Enable Auto DevOps                                                                                                                      | JupyterHub login                 | View function metrics (/../../../serverless/functions/*/my-function) |
| Install Kubernetes application<br>- Helm<br>- Ingress<br>- Cert Manager<br>- Prometheus<br>- GitLab Runner<br>- JupyterHub<br>- Knative | Upgrade Kubernetes application   | -                                                                    |
| Enable Slack application on gitlab.com                                                                                                  | Uninstall Kubernetes application | -                                                                    |
| Enable Slack slash commands                                                                                                             | -                                | -                                                                    |
| Use Auto DevOps templates (composable Auto DevOps)                                                                                      | -                                | -                                                                    |

See the corresponding [Periscope dashboard](https://app.periscopedata.com/app/gitlab/462967/Configure-Metrics) (internal).

## Auto DevOps

Our vision for “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)” is
to leverage our [single application](/handbook/product/single-application/) to
assist users in every phase of the development and delivery process,
implementing automatic tasks that can be customized and refined to get the best
fit for their needs.

With the dramatic increase in the number of projects being managed by software
teams (especially with the rise of micro-services), it's no longer enough to
just craft your code. In addition, you must consider all of the other aspects
that will make your project successful, such as tests, quality, security,
logging, monitoring, etc. It's no longer acceptable to add these things only
when they are needed, or when the project becomes popular, or when there's a
problem to address; on the contrary, all of these things should be available at
inception.

That's why we created Auto DevOps. We also made it composable so you can pick and choose which [stages](https://docs.gitlab.com/ee/topics/autodevops/#stages-of-auto-devops) to use our default templates in.

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)

[Learn more](/product/auto-devops/) • [Documentation](https://docs.gitlab.com/ee/topics/autodevops/) • [Vision](/direction/configure/auto_devops/)

## Kubernetes Configuration

Configuring and managing your Kubernetes clusters can be a complex, time-consuming task. 
We aim to provide a simple way for users to configure their clusters within GitLab; tasks 
such as scaling, adding, and deleting clusters become simple, single-click events.

[Learn more](/solutions/kubernetes/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/) • [Vision](/direction/configure/kubernetes_management/)

## Serverless

Taking full advantage of the power of the cloud computing model and container
orchestration, cloud native is an innovative way to build and run applications.
A big part of our cloud native strategy is around serverless. Serverless
computing provides an easy way to build highly scalable applications and
services, eliminating the pains of provisioning & maintaining.

[Learn more](/product/serverless/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) • [Vision](/direction/configure/serverless/)

## Runbook Configuration

[Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) will
allow operators to have real-time view into the happenings of their systems.
Building upon this concept, we envision rendering of runbook inside of GitLab as
interactive documents for operators which in turn could trigger automation
defined in `gitlab-ci.yml`.

[Documentation](https://docs.gitlab.com/ee/user/project/clusters/runbooks/) • [Vision](/direction/configure/runbooks/)

## ChatOps

The next generation of our ChatOps implementation will allow users to have a
dedicated interface to configure, invoke, and audit ChatOps actions, doing it in
a secure way through RBAC.

[Documenation](https://docs.gitlab.com/ee/ci/chatops/) • [Vision](/direction/configure/chatops/)

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through
machine-readable definition files, rather than manual hardware configuration or interactive
configuration tools. The IT infrastructure managed by this comprises both physical equipment
such as bare-metal servers as well as virtual machines and associated configuration resources.
The definitions are stored in a version control system. IaC takes proven coding techniques and 
extends them to your infrastructure directly, effectively blurring the line between what is an
application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all 
infrastructure related workflows in GitLab are well supported. Our initial focus will be on Terraform.

[Vision](/direction/configure/infrastructure_as_code/)

## Chaos Engineering

Chaos engineering in a powerful practice that allows operators to architect
powerful distributed systems that can withstand turbulent conditions. We want
operators to be able to run downtime scenarios randomly to test the resilience
of their architecture. Starting with the minimum units (pods) all the way the
largest units (regions).

[Vision](/direction/configure/chaos_engineering/)

## Cluster Cost Optimization

Compute costs are a significant expenditure for many companies, whether they
are in the cloud or on-premise. Managing these costs is an important function
for many companies. We aim to provide easy-to-understand analysis of your infrastructure
that could identify overprovisioned infrastructure (leading to waste), recommended changes,
estimated costs, and automatic resizing.

[Vision](/direction/configure/cluster_cost_optimization/)

<%= partial("direction/contribute", :locals => { :stageKey => "configure" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/product-management/process/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Configure at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Configure);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sections below.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "configure" }) %>
