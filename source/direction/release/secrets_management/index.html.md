---
layout: markdown_page
title: "Category Direction - Secrets Management"
---

- TOC
{:toc}

## Secrets Management

HashiCorp's [Vault](https://www.vaultproject.io/use-cases/secrets-management/) is paving the way for OSS Secrets Management and many of our customers are leveraging the solution today.  Vault lets you easily rotate secrets and can manage intermediate, temporary tokens used between different services. This ensures there are no long-term tokens lying
around or commonly used. Vault minimizes GitLab's attack surface and protects 
against any unknown zero-day vulnerabilities in our Rails app today or those that allow a bad actor to access the Gitlab server by ensuring that GitLab does not hold any long term secrets. 

See the [introduction to Vault](https://www.youtube.com/watch?v=pURiZLl6o3w) and our discussion on [Vault<>GitLab Integration](https://www.youtube.com/watch?v=9kD3geEmSJ8) with our CEO @systes.  

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecrets%20Management)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

## Secrets Management & Vault Use Cases 

There are three main secrets management use cases that Vault will solve for. These are as follows: 

- Keeping GitLab's own secrets safe as seen in [gitlab&1319](https://gitlab.com/groups/gitlab-org/-/epics/1319)
- Storing secrets for CI variables and jobs which can be followed in [gitlab&816](https://gitlab.com/groups/gitlab-org/-/epics/816)
- Managing secrets in general, in own Vault or one provided by GitLab, as illustrated by a collection of features including the Bundling of Vault with Gitlab ([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317), a GitLab.com provided instance ([gitlab#28584](https://gitlab.com/gitlab-org/gitlab/issues/28584)) and a built-in manager for Vault secrets ([gitlab# 20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)). 

## Target audience and experience

Operations, compliance, security, and audit teams will derive immense value from being able to manage secrets within GitLab. Vault will expand GitLab's security by offering an extra layer for tokens, keys, and other confidential data. This combination of tools will further establish GitLab's presence as an enterprise-grade, corporate solution for Release Management. 

## What's next & why

Now that we can effectively authenticate Vault with GitLab from [gitlab#9983](https://gitlab.com/gitlab-org/gitlab/issues/9983), and users can install Vault into a Kubernetes clusters via [gitlab#9982](https://gitlab.com/gitlab-org/gitlab/issues/9982), we are devoting more time to handling CI Variables from Vault. We will first enable users to authenticate into Vault by using a Java Web Token via [gitlab#207125](https://gitlab.com/gitlab-org/gitlab/issues/207125), which will be the first phase of successfull fetching and reading secrets from Vault to be instrumented in [gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321). 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is "Viable": (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- [Allow a user to install Vault in Kubernetes Cluster](https://gitlab.com/gitlab-org/gitlab/issues/9982) (Complete)
- [Generate JWT for authentication and provide it to CI jobs](https://gitlab.com/gitlab-org/gitlab/issues/207125) (12.10)
- [Fetch and pass secrets from Vault](https://gitlab.com/gitlab-org/gitlab/issues/28321) (13.0)
- [Create Identity API to pass tokens and secrets to GitLab Projects](https://gitlab.com/gitlab-org/gitlab/issues/24123)

## Competitive landscape

There are other secrets management stores in the market. There is a nice overview
of [Vault vs. KMS](https://www.vaultproject.io/docs/vs/kms.html) which contains a
lot of information about why we believe Vault is a better solution for secrets
management. We could consider in the future also supporting different solutions
such as KMS.

Additionally, [Vault Enterprise](https://www.vaultproject.io/docs/enterprise/) offers
additional sets of capabilities that will _not_ be part of the open source version
of Vault bundled with GitLab. This includes 
[replication across datacenters](https://www.vaultproject.io/docs/enterprise/replication/index.html),
[hardware security modules (HSMs)](https://www.vaultproject.io/docs/enterprise/hsm/index.html),
[seals](https://www.vaultproject.io/docs/enterprise/sealwrap/index.html),
[namespaces](https://www.vaultproject.io/docs/enterprise/namespaces/index.html),
[servicing read-only requests on HA nodes](https://www.vaultproject.io/docs/enterprise/performance-standby/index.html)
(though, the open source version does support [high-availability](https://www.vaultproject.io/docs/concepts/ha.html)),
[enterprise control groups](https://www.vaultproject.io/docs/enterprise/control-groups/index.html),
[multi-factor auth](https://www.vaultproject.io/docs/enterprise/mfa/index.html),
and [sentinel](https://www.vaultproject.io/docs/enterprise/sentinel/index.html).

For customers who want to use GitLab with the enterprise version of Vault, we need
to ensure that this is easy to switch to/use as well.

## Top Customer Success/Sales issue(s)

Adding a Vault instance to omnibus installations that can be used for customer
secrets is the right first place to start. Additional features will be added on,
but this will meet the goal of providing a secrets management solution with GitLab
([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317)).

## Top user issue(s)

Our most popular issue is managing Vault secrets inside Gitlab ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)). 

We are looking at including the Vault installation as part of the omnibus
([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317))
package, which comes for free bundled with GitLab. Having an installation guaranteed
available serves as a foundation upon which we can build a deeper integration for
[moving GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319)
as well. Adding the same capability to gitlab.com is possible, but quite complex so is
being researched in its own issue ([gitlab-org#28584](https://gitlab.com/gitlab-org/gitlab/issues/28584)).

Expanding the prescriptive use cases for Vault in GitLab prior to investing in development effort is currently being considered in [gitlab-org&2365](https://gitlab.com/groups/gitlab-org/-/epics/2365).  

## Top internal customer issue(s)

Internally, once the Vault integration is available we can begin moving some of
the secrets tracked internally in GitLab to the included Vault.

- [Examine which secrets we can move to Vault from gitlab.rb](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4297)
- [Discontinue using attr_encrypted](https://gitlab.com/gitlab-org/gitlab/issues/26243)
- [Allow the `db_key_base` secret to be rotated](https://gitlab.com/gitlab-org/gitlab/issues/25332)

The MVC for migrating our internal secrets is being tracked in the epic to [move GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319). Supporting GitLab internal secrets, does require the Vault integration being mandatory as part of the install first.

## Top Vision Item(s)

Secrets management is a must-have for enterprise-grade Release Governance. Adding a proper interface to Vault ([gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)) embedded in GitLab, making it easier to interact with the Vault instance. This would also help bridge the gap between manual and automated action wihin the UI. The interface can be leveraged for all secrets, which would also be a competitive feature set for the Operations-centered and security minded buyers within the regulated space.  
