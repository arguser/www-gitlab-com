---
layout: markdown_page
title: "Category Direction - Source Code Management"
---

- TOC
{:toc}

## Source Code Management

| Section | Stage | Maturity | Last Reviewed |
| --- | --- | --- | --- |
| [Dev](/direction/dev/) | [Create](https://about.gitlab.com/stages-devops-lifecycle/create/) | [Loveable](/direction/maturity/) | 2020-03-12 |

## Introduction and how you can help

The Source Code Management direction page belongs to the [Source Code](/handbook/product/categories/#source-code-group) group of the [Create](/stages-devops-lifecycle/create/) stage,
and is maintained by [James Ramsay](https://gitlab.com/jramsay).

This direction is a work in progress, and everyone can contribute.
Please comment and contribute in the linked issues and epics.
Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=repository)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=repository)

## Overview

<!--
A good description of what your category is.
If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it.
Please include usecases, personas, and user journeys into this section.
-->

Source Code Management provides the core workflows and controls for teams to collaborate using Git to build great software,
including protected branches, code owners, merge request approvals, and mirroring.

<!--
### Target Audience

An overview of the personas involved in this category.
An overview of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.
-->

<!--
### Challenges to address

- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

## Where we are Headed

<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this category once your strategy is at least minimally realized.
-->

Building great software depends on teams working well together.
Teams can rarely be divided into areas of complete independence.
As cross-functional security, compliance and growth teams are formed,
or new services and libraries are created,
effective coordination and collaboration is need.
This is true whether using a single monolithic repository,
or spread across numerous smaller services and libraries.

Teams require the controls to protect production while making it easy for everyone contribute.
This means providing more granular and dynamic controls so that low risk changes can be made easily,
and only the highest risk changes require the strictest controls.

Building software we stand not only on the shoulders of the giants of Computer Science,
but of our contemporaries: the authors and maintainers of open source software.
Private companies have been beneficiaries, contributors, and authors of these projects,
yet lack of controlled workflows prevent more upstream contributions.

Upstreaming contributions from private repository to a public upstream should simple and safe,
even for conservative organizations.
Whether the upstream repository is on the same GitLab server,
is hosted on GitHub.com,
or upstream contributions managed via a mailing list.
GitLab should enable collaboration between isolated and firewalled teams.

### What's Next & Why

<!--
This is almost always sourced from the following sections, which describe top priorities for a few stakeholders.
This section must provide a link to an issue or [epic](/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.
-->

- **Next:** [Multiple CODEOWNER sections](https://gitlab.com/gitlab-org/gitlab/issues/12137)

    Requiring specific people to review changes to certain areas of the
    codebase is helpful for compliance and quality. Using CODEOWNERS allows
    approval requirements to be dynamically added to merge requests based on
    the files changed.

    For large projects where multiple teams work on the same repository,
    allowing each team to define their own approval requirements in separate
    sections makes it possible to manage these competing interests without
    conflicts.

- **Next:** [Forking improvements](https://gitlab.com/groups/gitlab-org/-/epics/1421)

    Forking workflows are important for open source projects on public instances like GitLab.com,
    but they are also used for private projects on GitLab.com and elsewhere.
    There are a range of significant shortcomings in the forking workflow that should be resolved.
    Forking workflows should be fully supported in GitLab so that they can be used by open source projects and enterprises, public or private.

### What is Not Planned Right Now

<!--
Often it's just as important to talk about what you're not doing as it is to discuss what you are.
This section should include items that people might hope or think we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should in fact do.
We should limit this to a few items that are at a high enough level so someone with not a lot of detailed information about the product can understand the reasoning.
-->

- [Branch read access controls](https://gitlab.com/gitlab-org/gitlab-ee/issues/720)

    Limiting which branches a user can read in a Git repository is possible in a basic sense,
    by only advertising a subset of refs,
    but it is not possible to guarantee that unreachable objects will not be sent to the client.
    This means that branch read access controls would be very weak,
    since they could not prevent exfiltration of data they do not have permission to read.

- Path-level read access controls

    From a commit, Git expects all trees and blobs to be reachable.
    Although Git supports partial clone and spares checkout,
    which allow data to be excluded from fetch and checkout,
    Git expects to be able to fetch missing objects on demand.
    Deliberately excluding objects by path is likely to cause unexpected failures.

### Maturity Plan

<!--
It's important your users know where you're headed next.
The maturity plan section captures this by showing what's required to achieve the next level.
-->

This category is currently at the **Loveable** maturity level (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

However, specific aspects are not yet loveable:

- [Forking](https://gitlab.com/groups/gitlab-org/-/epics/264)
- [Mirroring](https://gitlab.com/groups/gitlab-org/-/epics/852)

## Competitive Landscape

<!--
Lost the top two or three competitors.
What the next one or two items we should work on to displace the competitor at customers?
Ideally these should be discovered through [customer meetings](//handbook/product/product-management/process/#customer-meetings).

We’re not aiming for feature parity with competitors,
and we’re not just looking at the features competitors talk about,
but we’re talking with customers about what they actually use,
and ultimately what they need.
-->

Important competitors are [GitHub](https://github.com) and [Perforce](https://perforce.com),
and increasingly [Azure DevOps](https://docs.microsoft.com/en-us/azure/devops/user-guide/alm-devops-features).

For public open source projects, GitHub is our primary competitor,
with millions of active users having chosen GitHub before the first version of GitLab ever existed.

In most source code management capabilities GitLab compares favorably to GitHub,
the most notable exception being the maturity of forking workflows which GitHub pioneered.
GitHub has a highly polished and fast product,
which makes tasks like browsing and managing projects fast and easy.

- [Make forking loveable](https://gitlab.com/groups/gitlab-org/-/epics/264)

For users of [SVN (Apache Subversion)](https://subversion.apache.org/) intending to migrate to Git,
GitHub is a significant competitor, particularly because GitHub supports hosting SVN repositories.

[Perforce](https://perforce.com) competes with GitLab primarily on it's ability to support enormous repositories,
however, Perforce also competes on the basis of being a Centralized Version Control System.
This means that Perforce not only supports granular write permissions,
but granular read permissions on a branch and file path basis.
While fine grained read permissions are important to some customers,
large monolithic repositories may be split into smaller repositories allowing read controls and easier management.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)

## Analyst Landscape

<!--
What are analysts and/or thought leaders in the space talking about?
What are one or two issues that will help us stay relevant from their perspective?
-->

Large file support (see [Gitaly direction](/direction/create/gitaly)) is an ongoing area of interest because it blocks certain segments of software development from using Git.

Similarly extremely large repository support (see [Gitaly direction](/direction/create/gitaly)) is also an area of interest for the same reason.

## Top Customer Success/Sales issue(s)

<!--
These can be sourced from the CS/Sales top issue labels when available,
internal surveys, or from your conversations with them.
-->

The most frequent category of request is for improved support for finer grained controls,
so that policies can be enforced at key points in the workflow,
and more permissive permissions can be granted at other times.

- [Merge request approval chains](https://gitlab.com/gitlab-org/gitlab-ee/issues/965) improves **efficiency** within restrictive control flows by allowing organizational hierarchy to be reflected in the merge chain, allowing incremental escalation of a merge request from a broad pool of more junior reviewers, up to the most senior reviewer.

Other items of frequent interest include:

- Support for very large repositories (see [Gitaly direction](/direction/create/gitaly)), requested particularly by customers migrating from a Centralized Version Control System.
- [Improved support for forking workflows](https://gitlab.com/groups/gitlab-org/-/epics/264), which is important for customers large and small, commercial and open source.

## Top user issue(s)

<!--
This is probably the top popular issue from the category (i.e. the one with the most thumbs-up),
but you may have a different item coming out of customer calls.
-->

- [Add project setting to set defaults and disable usage of squash merge](https://gitlab.com/gitlab-org/gitlab-ce/issues/49221)
- [Automatically create tag when merging](https://gitlab.com/gitlab-org/gitlab-ce/issues/22363)
- [Cascade merges requests into multiple branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/11648)
- [Clone statistics](https://gitlab.com/gitlab-org/gitlab-ce/issues/21743)

## Top internal customer issue(s)

<!--
These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding) the product.
-->

- [Subscribe to code changes](https://gitlab.com/gitlab-org/gitlab-ee/issues/1817)

## Top Vision Item(s)

<!--
What's the most important thing to move your vision forward?
-->

- Support for monolithic repositories (see [Gitaly direction](/direction/create/gitaly))
- [Distributed merge requests](https://gitlab.com/groups/gitlab-org/-/epics/260) will overcome the single server boundaries of merge requests, and allow teams on GitLab.com or self-hosted GitLab instances to merge their changes from one server to another.
