---
layout: handbook-page-toc
title: "Group Conversations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Group Conversations are optional recurring meetings providing regular updates from GitLab teams on a [rotating schedule](/handbook/people-group/group-conversations/#schedule). They are scheduled by [People Experience Associates](/job-families/people-ops/people-experience-associate/) and will automatically appear on your calendar. Everyone is invited to participate by adding questions and comments to the Google Doc linked in the calendar invite. Non-confidential group conversations are streamed live and shared publicly to our [GitLab Unfiltered YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e).

Over the years, GitLab has had Functional Group Updates and recently we've evolved FGUs to be more conversational and engaging.

In this video our CEO, Sid gives our team tips and tricks for their FGU. This still applies to Group Conversations.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/MN3mzvbgwuc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Below is a guide to help everyone get the most out of these conversations. If you have suggestions for how to make the attendee or meeting leader experience please create an MR to update this page — [everyone can contribute](/company/strategy/#why)!

## For Attendees

Attendance is optional. If you are unable to attend the Group Conversation at its scheduled time, don't let that hold you back! Watch the latest Group Conversation in the Google Drive folder found in the description of each update.  If you have questions or a discussion to start, bring it to the `#group-conversations` Slack channel! Make sure to @tag the presenter!

1. Please enter your questions or comments on the linked Google Doc. Preface your question with your full name (first and last name) because there might be other people with your first name on the call and it's helpful to newcomers if they are distinguished. Remember [First Post is a badge of honor](/handbook/communication/#first-post-is-a-badge-of-honor).
1. Keep in mind that Group Conversations are recorded and shared publicly and that it's okay to opt out of using your name due to safety and privacy concerns.
1. Please do not include customer names in your questions/comments.
1. Be ready to ask your question out loud as it comes up in the queue.
1. When speaking on the call, please say your name first, to help others know who is speaking.
1. Please don't use the Zoom chat to add information but add them to the doc so people reading the agenda after the call can also benefit.
1. Not everything has to be a question. If you have a comment, bias to putting it into the Google Doc so that those who weren't able to attend the meeting live can see what you had to say (and any responses that arose from it).
1. Thanking and recognizing people is very important.
1. You can ask someone to present a slide to get more context.
1. It's okay to add a question to the end of the queue in the Google Doc as the conversation is taking place.
1. You can contribute taking notes to transcribe the answers during the conversation. Thanks!
1. Make sure there are at least 5 empty items in the list (that just contain a space) for people to add new questions.

## For Meeting Leaders

### Scheduling

Calls are scheduled by [People Experience Associates](/job-families/people-ops/people-experience-associate/). If you need to reschedule, please *switch* your presenting day with another Group Conversation leader, by asking another leader in the #group-conversations channel publicly. People Experience Associates are not responsible for finding a replacement for your day. If you've agreed to switch, please do the following:
  - Go to the *GitLab Team calendar* invite
  - Update the date of your and the other invite to be switched
  - Choose to send an update to the invitees
  - _If prompted_ with the questions to update 1 or all events, choose to only update this event

### Preparation Tips

A little bit of preparation can go a long way in making the call worthwhile for everyone involved. People tend to spend at least an hour to prepare their updates.

#### Logistics

1. You can invite someone within your team to give the update. It doesn't need to be the team lead, but the team lead is responsible for making sure that it is given.
1. Make sure to add the link of the presentation to the Group Conversation Agenda Document invite at least 24 hours _before_ the call takes place. This allows team members to see the presentation, to click links, have [random access](https://twitter.com/paulg/status/838301787086008320) during the presentation,  and to quickly scan the presentation if they missed it. Please also add who the speaker will be to the presentation and the invite. People Ops will ping the appropriate team member at the 24-hour mark if the event hasn't been updated yet.
1. Drop links to the Group Conversation Agenda Document and the slides in the #company-announcement channel 24 hours before the call.
1. Consider blocking off the 30 minutes before your scheduled to lead a Group Conversation

#### Presentation

Use presentations to document everything people should know about your group. These presentations are for attendees to review and generate questions from for the call. You can also record and upload a [YouTube video](/handbook/marketing/marketing-operations/youtube/) if there is additional context that would be beneficial to supplement with the slides.

**Please remember that you should NOT present during a Group Conversation.** Synchronous calls are for conversation and discussion. It is the responsinility of the attendees to prepare questions before the call to maximize value of time spent synchronously. If someone does start to present the slides in the group conversation everyone is encouraged to say: 'At GitLab we use meetings for conversation and not presentation, but please do consider recording a video for next time.'.

There are three layers of content in a presentation:
  - Data, this is the contents of the slide.
  - Take away, this is the title of the slide, so use: 'migration 10 days ahead of schedule', instead of 'migration schedule estimates', the combined titles of your slides should make a good summary.
  - Feelings, this is the verbal and non-verbal communication in the video feed, how you feel about the take away, 'I'm proud of the band for picking up the pace'.

1. Save time and ensure asynchronous communication by writing information on the slides. Many people will not be able to participate in Group Conversations, either live or recorded, but can read through the slides.
2. Slides with a lot of text that can be read on their own with lots of links are appreciated.
3. Please add the link to your Group Conversation recordings folder in Google Drive to the invite. This enables team members to easily review the recording afterward.
4. If you want to present please consider [posting a recording to YouTube](/handbook/marketing/marketing-operations/youtube/) at least a day before the meeting, link it from the Google Doc, and mention it in the relevant slack channels.
5. Once a quarter, add a slide covering items being actioned from the engagement survey.
6. Use this [slide deck](https://docs.google.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing) as template to your presentation. Presentations should allow editing (preferred) or commenting from everyone at GitLab.

##### Examples of good presentations

- [Strategic Marketing Group Conversation 2020-01-22](https://docs.google.com/presentation/d/1xSxP_AztHHtLcYFd12Mf5yUIMVy9BU6QZnSebrlO7oo/edit): This has lots of links and a supporting YouTube presentation.

#### Cancellation

We try not to cancel a GC. 
We prefer to have half-prepared GCs over delaying them, but occassionally there may be a last-minute cancellation. 
If a Group Conversation is cancelled, the People Ops and People Experience teams should ask a member of the e-group to host an AMA.

### 24 Hours Before the Call

1. Ensure the slide deck is visible to all of GitLab
1. Give a heads up in #company-announcements on Slack if there is a video for the group conversation that you'd like people to watch beforehand.

### 30 Minutes Before the Call

1. Open the questions Google Doc linked from the invite and skim the questions to get a sense of what you can expect.
1. Enable “Show document outline” under the View menu to navigate the document more easily.
1. Improve the hygiene of the questions doc to use numbered lists instead of bulleted lists, so you can refer to questions by number if needed later.
1. Reduce distractions for yourself and the attendees by:
   - having the presentation open in its own new browser window, and only sharing that window in Zoom.
   - switching off notifications (from Slack, email, etc.). On Mac, in the notification center in the upper right hand corner, click on Notifications, select Do Not Disturb (sometimes you need to scroll up to see the option).

## During the Call

### Livestreaming the Call

All group conversations (Public and Private) will be [Livestreamed with Zoom](/handbook/marketing/marketing-operations/youtube/#live-streaming) to the [GitLab Unfiltered Channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e).

The `People Experience Associate` is responsible for starting the livestream, following the steps below:

1. Ensure there are 15 numbered lines below the date, topic, host name, and slides in the [Group Conversation Agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit) for questions to be added
2. Log in `5-minutes prior` to the start of the Group Conversation from the People Ops Zoom account. You can double check to ensure you are logged in to the People Ops account by going to [Zoom](zoom.us/signin) and verifying that the tanuki logo appears in the upper-righthand corner.

_Important: The DRI for the Group Conversation also has hosting rights. Zoom will automatically assign the first person (DRI or People Ops) who logs into the call as `host`. It is necessary to have `host` rights (**`co-hosting` rights are not sufficient**) in order to be able to livestream the call to YouTube. If the DRI logs in before you, please ask them to transfer you host rights by clicking `Manage Participants` at the bottom of the Zoom page, hover over the `GitLab Moderator` participant, click the `More` dropdown menu, and select `Make Host`._ 

3. Turn off Zoom recording (which will turn on automatically)
4. Ensure the clock on your computer is enabled to include seconds by clicking the Apple icon in the upper lefthand corner of your screen, selecting `System Preferences`, opening `Date and Time` preferences, and checking `Display the time with seconds`. 
5. Login to the [GitLab Unfiltered YouTube Channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e) account 60 seconds prior to the meeting start time to prepare for the livestream.

_Important: It is recommended to log into YouTube **no more than ~60 seconds prior** to the start of the livestream. People Experience Associates have had permission issues with livestreaming in the past when logging into YouTube several minutes prior to the start of the livestream._

6. Start the Zoom recording to record to the cloud `exactly` at the meeting start time and ask the DRI "`May I start the countdown for this public/private livestream?`" (_Note: Be sure to verify in the calendar invitation whether the meeting is publicly streamed or privately streamed_)
7. As soon as the DRI confirms it is ok to proceed, state "`I will start the countdown for this public/private livestream in 3, 2, 1...`" and stream to YouTube directly from Zoom by clicking `More` in the lower righthand corner of the screen and selecting `Live on YouTube`
- Helpful screenshots are available [here](/handbook/marketing/marketing-operations/youtube/)
- If you experience the error "Please grant nesessary privilege for live streaming" click on the "Not me" option that appears beside the GitLab Unfiltered account on the middle of the page. You will then be prompted to sign in again, please do so and click "Go Live". 
8. When the call ends, stop the Zoom recording
9. Stop the Youtube livestream by going to [YouTube Studio](https://studio.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) - `Videos` - `Live` - select the circular icon to `View in Live Dashboard`. This will trigger the video to stop livestreaming and appear `Offline`. 


Additional Reminders:
- Close or mute the YouTube page to avoid an echo; you do not need to monitor it during the presentation. Zoom will show *Live on Youtube* on the meeting.
- Announce that the livestream is live and is public or private in the call.
- Remember to only share the slides if illustrating something, otherwise let the speakers be visible in **speaker** view setting on zoom.
- End the zoom call for everyone immediately at the end of the meeting as it subsequently ends the livestream. Press End Meeting and then End Meeting for All.
The calendar events for all livestreamed Group Conversations should adhere to our YouTube [visibility guidelines](/handbook/communication/youtube/#visibility).
Note: If you will be out of office on the day of your Group Conversation, or need a person other than the DRI listed in the [Schedule & DRI table](/handbook/people-group/group-conversations/#schedule--dri):
- Please update the Alternate Host line in the Group Conversation Agenda document.
- Please notify any of the People Experience Associates in the #group-conversations Slack channel so that they may change the host in Zoom's settings.

### Other things to Note

1. Right now, everyone at GitLab the company is invited to each call, we'll invite the wider community later.
1. Make sure there is a list of 15 numbered items that only contains a space, this will make it easier to add things.
1. Join the call 2-3 minutes early to ensure you're ready when it starts.
1. Right before the call begins (5 seconds or so before the top of the hour), the call moderator will press the Record button and everyone will hear "This call is being recorded".
1. If the call is to be livestreamed, the moderator will ask: "May I start the Countdown?" The call host will either verbalize yes or give a thumbs up. The moderator will state: "Thank you, I'll start the countdown for this Public/Private Livestream in 3,2,1" and the moderator will then start the livestream following the [handbook directions](/handbook/marketing/marketing-operations/youtube/#livestream-with-zoom).
1. When the meeting starts introduce yourself and say a few words about what this call is about. For example, "Hello everyone, I'm Diane and I lead the [team]. I am looking forward to answer your questions about our [group name] group."
1. Do not present your slides. Invite the first person to verbalize their question, respond and when relevant mention a slide number. This is similar to what we do during [board meetings](/handbook/board-meetings/).
1. It is okay to wait until at least two questions have been entered before starting to answer questions, and allow some time for folks to read the slides at the start of the call.
1. If someone can't verbalize their question (not in call, driving, audio problems) read it aloud for people watching only the video.
1. Tone should be informal, like explaining to a friend what happened in the group last month, and shouldn't require a lot of presentation.
1. It's the responsibility of the team members of the group to ensure the content is distributed, this includes ensuring appropriate notes are taken in the [Group Conversation Agenda](https://docs.google.com/document/d/1zELoftrommhRdnEOAocE6jea6w7KngUDjTUQBlMCNAU/edit)
1. This meeting is scheduled to be 25 minutes long. Please keep an eye on the clock and end the meeting on schedule. This meeting must end no later than 29 minutes after the hour.
1. It is appropriate to end the call early if there are no more questions. We aim for results, not for spending the time allotted. Please avoid the temptation of presenting slides to fill time.
1. Don't do a countdown when you're asking for questions, people will not ask one. Just end the call or even better say: we'll not end the call before getting at least one question.
1. If there are more questions or a need for longer conversation, mention on what chat channel the conversation can continue or link to a relevant issue.

## After the Call

1. All calls are published live to YouTube, with the exception of Finance, Sales, Security, Channel, and Partnerships. Every call is reviewed live to change livestream to private should anything confidential arise.
1. If streaming live is not available, ensure the call is [uploaded to Youtube](/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube) and then announced in the appropriate slack channel. (i.e. `#company-announcements`, `#apac`) 
1. The conversation is also announced on and the recording linked from our company call agenda.
1. Calls are 5 times a week 30 minutes before the company call, 8:00 to 8:25am Pacific.
1. The call is recorded automatically, and all calls are transferred every hour to a Google Drive folder called "GitLab Videos", which is accessible to all users with a GitLab.com e-mail account.
1. Video recordings will be published on our blog so contributors, users, and customers can see it. We're aiming to publish a blog post once a week of that weeks' recordings with the matching slides in one go.
1. Slides with a lot of text that can be read on their own with lots of links are appreciated.
1. GC DRIs are encouraged to add uploads to the [Group Conversations playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpUeT6ozUTatC-JbNoJCC-e) within the GitLab Unfiltered YouTube channel. You may also use Google to [search for past Group Conversations](https://www.google.com/search?&hl=en-us&biw=1366&bih=921&tbm=vid&ei=HFzuXc7WLYO3ggeMxIG4BQ&q=gitlab+unfiltered+group+conversation&oq=gitlab+unfiltered+group+conversation&gs_l=psy-ab.3..33i160k1.23243.31186.0.31344.38.31.0.7.7.0.144.2401.25j5.30.0....0...1c..64.psy-ab..1.36.2340...0j0i8i30k1j0i30k1j33i299k1.0.45wyTBKz0bI). 


## Template for the blog post

For instructions on how to create a blog post, please see our [Blog Handbook](/handbook/marketing/blog/#create-a-new-post).

Please copy the code block below and paste it into a blog post with the file name `yyyy-mm-dd-group-conversation.html.md`.

```md
---
title: "GitLab's Functional Group Updates - MMM DD-DD" # replace "MMM" with the current month, and "DD-DD" with the date range
author: Name Surname
author_gitlab: gitlab.com-username
author_twitter: twitter-username
categories: Functional Group Updates
image_title: '/images/blogimages/functional-group-update-blog-cover.jpg'
description: "The Functional Groups at GitLab give an update on what they've been working on"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Company call](/handbook/#company-call), a different Group initiates a [conversation](/handbook/people-group/group-conversations/) with our team.

The format of these calls is simple and short where attendees have access to the presentation content and presenters can either give a quick presentation or jump straight into the agenda and answering questions.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### XXX Team

[Presentation slides](link-to-slides-deck)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/8vJBc8MJihE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!

```

Presentation - https://example.com [Input your presentation's URL and check if its set to view only for anyone with the link if using Google Slides]

- After the video is done uploading, click "Video Manager" in the bottom right corner.
- Edit the video to start when the meeting actually starts:
  - Click **Edit** next to the video icon.
  - Click the **Enhancements** tab on the top menu bar.
  - Click **Trim** on the bottom right. Slide the left edge of the bar to a few moments before the presentation begins, and the right edge of the bar to a few moments after the presentation ends. Click **Done**.
- Take a screenshot of the second slide of the presentation to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the **Info and Settings** tab when you are editing a video.
- After the video is finished being edited, change the security level back to "public".
- Make sure to use correct format for [embedding videos from YouTube](/handbook/engineering/ux/technical-writing/markdown-guide/#display-videos-from-youtube) into the blog post. Replace the video URL only.

## Schedule & DRI

There is a rotating schedule with each functional group having a conversation on a regular interval. 
We usually do not have Group Conversations or Company calls between December 23rd and January 2nd. We also tend to cancel these during [Contribute](https://about.gitlab.com/company/culture/contribute/).
The schedule with directly responsible individuals (DRI) is as follows:

| Week | Day | Group Conversation | DRI |
|---|---|---|---|
| One  | Mon  | [Secure & Defend Section](/handbook/product/categories/#secure--defend-section)  | David DeSanto  |
| One  | Tue  | [Ops Section](/handbook/product/categories/#ops-section)  | Kenny Johnston  |
| One  | Wed  | [Revenue Marketing](/handbook/marketing/revenue-marketing/)  | Evan Welchel  |
| One  | Thur  | [Marketing](/handbook/marketing/)  | Todd Barr  |
| One  | Fri  | [Channels](/handbook/sales/channel/)  | Michelle Hodges  |
| Two  | Mon  | [Enablement](/handbook/engineering/development/enablement/)  | Josh Lambert  |
| Two  | Tue  | [Dev Section](/handbook/product/categories/#dev-section)  | Eric Brinkman  |
| Two  | Wed  | [Quality](/handbook/engineering/quality/)  | Mek Stittri  |
| Two  | Thur  | [UX](/handbook/engineering/ux/)  | Christie Lenneville  |
| Two  | Fri  | Open  | Open  |
| Three  | Mon  | [Growth Section](/handbook/product/categories/#growth-section)  | Scott Williamson  |
| Three  | Tue  | [Support](/handbook/support/)  | Tom Cooney  |
| Three  | Wed  | [Development](/handbook/engineering/development/)   | Christopher Lefelhocz  |
| Three  | Thur  | [Finance](/handbook/finance/)  | Paul Machle  |
| Three | Fri | Open  | Open  |
| Four  | Mon  | Open  | Open  |
| Four  | Tue  | [General](/handbook/ceo/)  | Sid Sijbrandij   |
| Four  | Wed  | [CRO Group Conversation](/handbook/sales/)  | Michael McBride  |
| Four  | Thur  | [People](/handbook/people-group/)  | Carol Teskey and Dave Gilbert  |
| Four  | Fri  | Open  | Open  |
| Five  | Mon  | [Product](/handbook/product/)   | Scott Williamson  |
| Five  | Tue  | [CI/CD Section](/handbook/product/categories/#cicd-section)  | Jason Yavorska  |
| Five  | Wed  | [Strategic Marketing](/handbook/marketing/product-marketing/)  | Ashish Kuthiala  |
| Five  | Thur  | [Infrastructure](/handbook/engineering/infrastructure/)  | Gerir Lopez-Fernandez  |
| Five  | Fri  | [UX Research](/handbook/engineering/ux/ux-research/)  | Sarah O'Donnell  |
| Six  | Mon  | [Security](/handbook/engineering/security/)  | Jan Urbanc  |
| Six  | Tue  | [Meltano](/handbook/meltano/)  | Danielle Morrill  |
| Six  | Wed  | [Alliances](/handbook/alliances/)  | Brandon Jung  |
| Six  | Thur  | [Community Relations](/handbook/marketing/community-relations/)  | David Planella  |
| Six  | Fri  | Open  | Open  |

## Request a new Group Conversation

- Review existing Group Conversation schedule and find an empty time slot.
- Create an MR to add new Group Converstaion session to this empty slot. Include GC Name, link to the handbook page, and also include DRI name.
- Assign the MR to the People Experience team for review, merging, and creation of the Zoom link and addition to the GitLab Team Meetings calendar.

## Request an AMA

- People Experience Associates (with support as needed from People Operations Specialists) manage the GitLab Team Meetings calendar and their events.
- People Experience Associates host Group Conversations and AMAs. If an AMA is created with short notice and there are no People Ops team members available, then People Ops will not be able to host or livestream.
- To request an AMA be put on the GitLab team meetings calendar, the requestor should find a date and time on the GitLab Team Meetings calendar that works for the AMA host. 
- The requestor will then ping the People Experience team (using the Slack group handle `@people_exp`) in the `#peopleops` Slack channel.
- The requestor will ask for the AMA to be put on the GitLab team meetings calendar, providing the following in the request:
  - Date and Time (with Time Zone)
  - Title of the AMA
  - Public or Private Livestream
  - Alternate host(s) 
  - Link to AMA document
- The People Experience Associate will create the event in the PeopleOps Zoom account and add it to the GitLab team meetings calendar. The PEA will add the AMA document link in the description and invite `everyone@domain.com` to the meeting.

## Agenda

A possible agenda for the call is:

1. Accomplishments
1. Concerns
1. Stalled/need help
1. Plans
1. Questions and discussions live or in chat
