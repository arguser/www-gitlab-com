---
layout: handbook-page-toc
title: "Evangelist Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

At GitLab our mission is to change all creative work from read-only to read-write so that **everyone can contribute**. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we encourage everyone to contribute to its growth.

There are many ways to participate in the GitLab community today: [contributing to an open source project](/handbook/marketing/community-relations/code-contributor-program/), [contributing to our documentation](https://docs.gitlab.com/ee/development/documentation/), [hosting your open source project on GitLab](/solutions/open-source/), or teaching your colleagues and collaborators about the value of [Concurrent DevOps](/concurrent-devops/).

We are building an evangelist program to support people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. We will be announcing more in Q1. For now, please email `evangelists@gitlab.com` if you have feedback on our vision, ideas for how we can build our community, or suggestions for a name for our evangelist program.

## How to see what we're working on

We use the `Evangelist Program`, `Heroes`, and `Meetups` labels to track issues. The [Evangelist Program issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/951386?&label_name[]=Evangelist%20Program) provides an overview of these issues and their status.

Evangelist Program issues typically exist in the `Evangelist Program` subgroup but they can also exist in Field Marketing, Corporate Marketing, or other marketing subgroups. Issues related to our OKRs are listed in the Q4 OKR Epics: [Grow Heroes Program](https://gitlab.com/groups/gitlab-com/marketing/community-relations/evangelist-program/-/epics/7) and [Grow Heroes Activity](https://gitlab.com/groups/gitlab-com/marketing/community-relations/evangelist-program/-/epics/6). Our upcoming Meetups are tagged with the `Meetups` label and listed on the [Meetups board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?&label_name[]=Meetups).

We plan to replicate this process of having a epic for future OKRs and using labels and boards to track ongoing work related to our KPIs (for example: Meetups). As our OKRs and KPIs change, this page will be updated so you can see what we are working on and track our progress.

### Evangelist Program OKRs and KPIs

We are actively looking at ways to improve the data collection for our Evangelist Programs. If you have ideas for how we can improve, please reach out to [evangelists@gitlab.com](mailto:evangelists@gitlab.com).

#### [Q1 OKR: Improve scalability and capacity of the Evangelist Programs.](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/7)
1. Launch streamlined swag ordering and delivery via Printfection for meetup organizers and GitLab Heroes
1. Create new applications and workflows for GitLab Meetups and GitLab Heroes that allow us to capture all necessary information in the application, automate email responses, and reduce response times.
1. Create email mailing lists for both Meetups program participants and GitLab Heroes to allow for more efficient communication and better tracking.

#### [Q1 OKR: Improve diversity of Heroes program as defined on GitLab’s Diversity and Inclusion page.](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/6)
1. Create 3 new initiatives, campaigns, or content (such as blog posts or videos) aimed at improving the diversity of the Heroes program.
1. Accept 5 new GitLab Heroes who are gender minorities and 5 new GitLab Heroes from other underrepresented groups.

#### GitLab Meetups per month [KPI](/handbook/ceo/kpis/) Definition

We aim to participate in at least 20 GitLab Meetups per month. A GitLab Meetup is defined as a meetup with a presentation given by a GitLab team member or a wider community member about GitLab or a tangential topic. It does not include meetups without GitLab content where GitLab only provides support.

This KPI is tracked by counting the number of issues in the GitLab.com data with the [Meetups label](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name[]=Meetups) in the Marketing group. Each event should have one associated issue with the `Meetups` label. The issue due date should be set to the event date. The combination of `Meetups` label and due date will be used by our data dashboard to count the number of meetups each month. 
This method of tracking can results in errors when meetup issues are created in other groups or when GitLab-related presentations at meetups are not shared with the Community Relations team. 

<embed width="100%" height="100%" src="<%= signed_periscope_url(chart: 7799761, dashboard: 431555, embed: 'v2') %>">

### Upcoming events

The [Community Events calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) includes a list of our upcoming meetups, hackathons, office hours, and other community events. After opening the calendar, you can click on the `+` in the bottom right corner of the browser window to add `Community Events` to your list of calendars in Google Calendar.

## Meetups

**IMPORTANT: Due to the COVID-19 pandemic, GitLab is not currently supporting in-person meetups in order to encourage responsible social distancing within our community. We encourage organizers who wish to continue to bring their communities together using remote meeting platforms like Zoom and Google Hangouts.**

GitLab supports team members and members of the wider GitLab community who want to organize or speak at meetups. Our goal in supporting these events to better engage with and increase connections among the GitLab community, increase awareness of GitLab, and better educate the technology community.

### Organize a meetup

- We love and support meetups. If you participate in local tech groups and are interested in having a GitLab speaker or GitLab as a sponsor, please submit an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request). Please note, providing sufficient lead time (at least a month) allows us to better support your event so feel free to reach out as soon as possible.
- When a meetup issue is submitted by a community member, the issue should be assigned to the Evangelist Program manager, labeled with the [`Meetups`](https://gitlab.com/groups/gitlab-com/marketing/-/issues?label_name%5B%5D=Meetups) label, and be given a due date on the date of the event. Meetup issues created by a GitLab team member will have those attributes added via the template.
- If you are interested in creating your own GitLab meetup or if you already have an existing meetup on meetup.com that you would like to link to GitLab's meetup.com Pro account, please email `evangelists@gitlab.com`.  You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/pro/gitlab).
  - Connecting your group to GitLab's Meetup Pro instance will also allow you to utilize easy-to-use, custom templates for your Meetup events. When scheduling a new event on Meetup, you can choose a template by clicking on `Start from a template` in the right sidebar and choosing a template that best fits your event. 
- When you are getting started, we recommend scheduling at least 2 meetups. Publish your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meet-ups is to help build momentum in the new group.
- Often, we try to have GitLab employees attend the first couple of meetups. Once the group has hosted a couple of successful events, it becomes easier for the group to sustain itself. It is much harder to start new meetups versus maintaining existing ones. So we make an effort to support and keep new meetups going.
- Reach out to other like-topic meetups and invite them to your meetup to help grow your community.
- Once you have scheduled your meetup, add the event to our [events page](/events/) so the whole world knows! Check out the [How to add an event to the events page](/handbook/marketing/corporate-marketing/#how-to-add-events-to-the-aboutgitlabcomevents-page) section if you need help on how to add an event.
- We recommend all Meetup organizers record their events, when possible. Options include:
  - Using Quicktime to simultaneously conduct screen recording and audio recording. The two recordings can be synced into a single video file.
  - To record the speaker, we recommend using a webcam with a microphone (such as the [Logitech C922x](https://www.amazon.com/Logitech-C922x-Pro-Stream-Webcam/dp/B01LXCDPPK/ref=sr_1_2?crid=13ZBFCYG30ULV&keywords=logitech+webcam&qid=1552336114&s=gateway&sprefix=logitech+%2Caps%2C142&sr=8-2)). You can [insert the slides as images](https://support.apple.com/en-us/HT204674) into the video recording later using iMovie or your preferred video editing software.
- We love to collect photos and video clips from our organizers to highlight on our website, in company calls, and other materials. Send photos and video clips to evangelists@gitlab.com.
- When recording video clips of meetups, please follow these guidelines: 
  - Provide three video clips
  - Each video clip should be at least 90 seconds long each
  - Shoot video in landscape mode
  - Please do not use filters on submitted videos
  - Make sure everyone in the video is aware of how the video will be used and that they will be featured in a GitLab video

#### Meetup Expenses

**IMPORTANT: Due to the COVID-19 pandemic, GitLab is not currently supporting in-person meetups in order to encourage responsible social distancing within our community. In order to reinforce this policy, we will not be reimbursing meetup expenses for in-person events at this time. We encourage organizers who wish to continue to bring their communities together using remote meeting platforms like Zoom and Google Hangouts.**

<details><summary>This initiative is temporarily suspended due to COVID-19</summary>
- ~~GitLab can help cover the cost of food & beverage (and other expenses required for a successful event) for the meetup event.  A general guideline is $US 5/person for a maximum of $US 500 per each meetup.~~
- In order to ensure we can support meetup groups around the globe and to encourage our organizers to invest in producing high-quality events for our community, GitLab will only provide financial support and swag for two meetup events per month for each meetup group.  
- When making purchases for a meetup, please follow GitLab's company guideline to ["spend company money like it is your own money"](/handbook/spending-company-money/). Keeping the cost of individual meetups low is important as it will allow us to support a larger number of groups and events. 
- GitLab uses Tipalti to process invoices for meetups. To be reimbursed for your expenses, please send an email to [evangelists@gitlab.com](evangelists@gitlab.com) and [ap@gitlab.com](ap@gitlab.com) to request access to Tipalti. 
- Once you have access to the Tipalti portal, you will upload your invoice and receipts as a single document on the Tipalti portal or email the invoice and receipts as a single document to [gitlab@supplierinvoices.com](gitlab@supplierinvoices.com). We suggest organizers use our [Meetup Invoice template](https://docs.google.com/spreadsheets/d/1D3lpPrwfz1zQp8LsiWUq64aBNYHnlKbhjyPf6sQ8NsM/edit?usp=sharing).
- To create a single document containing your invoice and expenses, please follow these steps: 
  - Make a copy of the [Meetup Invoice template](https://docs.google.com/spreadsheets/d/1D3lpPrwfz1zQp8LsiWUq64aBNYHnlKbhjyPf6sQ8NsM/edit?usp=sharing).
  - Enter all your expenses into the template.
  - Download the sheet as a PDF.
  - Scan your receipts and add them to the PDF ([instructions for Mac](https://support.apple.com/en-us/HT202945)).
  - Upload the single document containing the receipts and invoice to Tipalti or send it to [gitlab@supplierinvoices.com](gitlab@supplierinvoices.com).
- Submission of an invoice will trigger a workflow that generates an email to the Evangelist Program Manager. Upon receipt of that email, will review the bill and take appropriate action:
  - If the report is complete and the expense is approved, the Evanglist Program Manager should click the `Approve bill` button in the mail.
  - If the bill's `Class` does not include the appropriate finance tag (for example: `Q4_FY2020_Meetups`) or there are other issues with the bill, the Evangelist Program Manager should click `Send back to AP`. 
  - If the bill is not legitimate, the Evangelist Program should click `Dispute bill`. 

For GitLab Team Members using Expensify for meetup-related expenses:
- If an expense is submitted via Expensify, the campaign finance tag (for example: `Q4_FY2020_Meetups`) should be selected as a `Classification` when creating the expense.
</details>

#### Meetup Swag

GitLab Meetup Starter Kits containing t-shirts, stickers, and info cards are available in Printfection. 

For GitLab team members, please follow these steps to provide meetup organizers with a link to order a GitLab Meetup Starter Kit: 
- Navigate to the `GitLab Meetup Starter Kit` campaign in Printfection. 
- To access the page, you will need to be logged in to Printfection using the credentials in 1Password. 
- On the page, click the green button labeled `+ GET NEW LINK`. 
- Copy the link generated in the popup window and share it with the Meetup organizer. Then click the button labeled `MARK LINK AS SENT` inside the popup. 

For Meetup organizers:
- If you require swag for a meetup and have not received a link, please comment on the issue for your meetup and request a link. 

### Speak at a meetup

Meetups help us raise awarness of GitLab and build communities in new places. We love to track them to know where the community is growing. If you are speaking at a meetup as a representative of GitLab or you are giving a talk about GitLab, please let us know! Here's how and why we do this:

- Speakers should use the [Meetup Speaker template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-speaker) to create a new issue with the details for your meetup talk.
- Opening an issue helps us support you. A few ways we can help:
  - Marketing: We'll share the event on our blog, via social, and with GitLab community members in the area to raise awareness of the event.
  - Sponsorship: GitLab will cover the cost of food and beverages for meetup organizers.
  - Swag: Our swag is pretty popular so we'll send you plenty of stickers and *maybe* some other fun stuff to give away.
  - Speaker prep: If you need help with your deck or would like someone to offer feedback on a dry run of your talk, the [Evangelist Program Manager](mailto:evangelists@gitlab.com) is happy to help.
- We track meetups via issues on the [Meetups issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name[]=Meetups). Take a look and see what we have coming up!

### Other meetups

We use Zapier to scan Meetup.com and Twitter for GitLab events that occur outside the scope of our program. Events containing "GitLab" on Meetup.com or tweets containing "GitLab Meetup" are added to the [GitLab Meetups](https://docs.google.com/spreadsheets/d/1pgXbLnquacqgMkT3T1-Q3EN5cr1Fq0SgCMwda43-Hdk/edit?usp=sharing) sheet and shared in Slack in the [#gitlab-meetups-feed](https://gitlab.slack.com/messages/CHHC8PFNF) channel. We review these events each week and reach out to thank organizers and speakers who are raising awareness for GitLab. In many cases, these organizers and speakers will also be offered support with future GitLab-related events.

## GitLab Heroes

[GitLab Heroes](/community/heroes/) engages, supports, and recognizes members of the wider GitLab community who make outstanding contributions to GitLab and our community around the globe. Examples of activities that may make a member of the wider GitLab community eligible for the Heroes program include:
* Organizing meetups
* Recording demos for YouTube
* Giving talks at conferences and events
* Writing technical blog posts
* Contributing to our open source project

Heroes are eligible for rewards to help enable and encourage contributions. These rewards include:
* Invitations to special events including GitLab Commit
* Support for travel to speak about GitLab at events
* GitLab Gold and Ultimate licenses
* Special Heroes swag so people know how awesome you are
* Access to GitLab's product, engineering, and technical evangelism teams to help with reviews of talks and blog posts

We have three levels of Heroes: Contributor, Hero, Superhero. A community member's contributions will determine at which level they enter the program and the benefits for Heroes increase as they progress through the levels. More detail can be found in the [Heroe's Journey](/community/heroes/#heroes-journey) section of the Heroes page.

Community members who are interested in applying for the Heroes program should apply through the [application form](/community/heroes/#apply) on the Heroes page.

The Heroes program is managed by the Evangelist Program Manager with support from the GitLab's Technical Evangelism team. The Evangelist Program Manager leads the review of applications along with the Technical Evangelism team. The Evangelist Program Manager also serves as the main point of contact with the Heroes community, manages Heroes related marketing pages and events, and is responsible for the adminstration of the program including metrics and KPI tracking.

Select Heroes may be asked to join the GitLab Technical Evangelism Community (TEC). At that point, those community members will be supported by and engage with the Technical Evangelism team.

Please email us at [evangelists@gitlab.com](mailto:evangelists@gitlab.com) if you have questions about the GitLab Heroes program.

### Communication with Heroes

Communication with Heroes applicants and members of the GitLab Heroes program is conducted through email updates, generally from the Evangelist program, and via the [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes). Please communicate using MRs and Issues in the GitLab Heroes project whenever possible to provide transparency to the wider GitLab community and allow for easier collaboration. 

When communicating with GitLab Heroes, you may wish to use the [email templates](https://docs.google.com/document/d/12XcQft1QTTM_fxDgGG4sPN3cRedkP5PFb_YW0OrGu-Q/edit?usp=sharing) created for acceptances, declines, follow-ups for clarification on contributions, and regular program updates. Access to these templates is limited to GitLab employees due to the sensitivity of some of the content in the templates (such as codes for Heroes swag). 

#### Requests for Heroes

Please follow the process detailed in the GitLab Heroes project to [request support from Heroes](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes#requesting-support-from-heroes). 

### GitLab Heroes Application Process 

To apply for GitLab Heroes, please complete the application form on the [GitLab](/community/heroes) Heroes page. The review committee consists of the Evangelist Program Manager as the lead with the Code Contributor Program Manager and the Senior Manager, Technical Evangelism as reviewers. The composition of the review committtee is subject to change. Any changes will be reflected here.

Generally, you will receive a response from GitLab within two weeks of submitting an application. This will either be an update on the status of your application or a request for additional information. If additional information is required, you can expect an update on the status of your application within two weeks of the receipt of that info. Following the review process, the Evangelist Program Manager will inform folks who are selected to be GitLab Heroes of your status including the level (`Contributor`, `Hero`, `Superhero`) at which you will enter the program. If you have any questions about the status of your application, please reach out to [evangelists@gitlab.com](mailto:evangelists@gitlab.com) for support.

We also conduct semi-annual reviews of the current GitLab Heroes members to ensure they are remaining active in the community in order to maintain their GitLab Heroes status. When a current GitLab Hero is inactive, the Evangelist Program Manager will contact them to identify the reasons and work with them to resolve any blockers or issues. When a GitLab Hero no longer plans to remain active in the GitLab community, the Evangelist Program Manager will remove them from the active membership of the program. 

### Adding yourself to the Heroes page 

Upon acceptance, Heroes are asked to submit a Merge Request to add themselves to the [GitLab Heroes members page](/community/heroes/members).

To add yourself to the Heroes page, you will need:

* Your personal Twitter / GitLab handles
* A picture of yourself for the Heroes Members page
  > **Picture Requirements**
  >
  > * Crop image to a perfect square.
  > * Keep maximum dimension under 400 by 400 pixels.
  > * Use the JPEG (`.jpg`) or PNG (`.png`) format.
  > * Test image in color and black-and-white.
  > * Name file `yournameinlowercase` and add the appropriate file extension.


Once you have the above items, follow these steps to add yourself to the Heroes page:
  1. Go to the [Heroes file in the GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/heroes.yml) project.
  1. On the file page, click on the button labeled `Web IDE` near the middle of the page.
  1. When prompted, click `Fork` to create a Fork of the repo which will allow you to make changes and submit a Merge Request.
  1. You should see the `heroes.yml` file open in your browser once the fork has been created. Add the following fields to the end of the file and enter your information into each of the blank fields:
```yaml
- type: person
  name:
  locality:
  country:
  role: GitLab Hero
  twitter:
  bio:
  gitlab:
  picture:
```
  1. After updating `heroes.yml`, use the file browser on the left side of the screen to navigate to `source/images/heroes`.
  1. Click the `⋁` icon next to the `heroes` directory, select upload file, and upload the photo of yourself. Be sure to follow the picture requirements listed above and confirm that the file name matches your `picture` entry in `heroes.yml`.
  1. Once you have finished this, click the `Commit` button in the bottom left. It should say something like `2 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
  1. Check the files to ensure your updates are what you expect. If they are, click the check mark next to the filename to "stage" these changes.
  1. Once you have verified all of the edits, enter a short commit message including what you've changed. Choose `Create a new branch`. Name the branch in the format of `YOURINITIALS-heroes-page` or similar. Tick the `Start a new merge request` checkbox. Then click `Commit` once more.
  1. Click on the Activity link in the header to go to your Activity page. Once there, click on the blue `Create merge request` button at the top of the page.
  1. Fill out the merge request details. Please ensure you tick the box to `Allow commits from members who can merge to target branch` as detailed on the [Allow collaboration on merge requests across forks](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members) page in our docs.
  1. Mention `@johncoghlan` in a comment in the merge request so our team can review and merge.

### GitLab Heroes Licenses

GitLab team members can issue a Gold or Ultimate license to GitLab Heroes by following the steps below. These licenses are only open to GitLab Heroes at the Hero or Superhero level. Licenses should be issued for 6 months and can be extended as long as the GitLab Hero maintains their Hero or Superhero status in the program.

#### Self-managed
   1. If you need access to license.gitlab.com, you will need to get permission to login to dev.gitlab.org by creating an [access request issue](https://gitlab.com/gitlab-com/access-requests/issues).
   1. Log in to [license.gitlab.com](https://license.gitlab.com/) with your GitLab credentials.
   1. Choose the manual entry option.
   1. Select the Ultimate license (unless requested otherwise).
   1. Set the term for 6 months.
   1. Set the appropriate number of seats to 1.
   1. Indicate that the license is for a GitLab Hero and that use of the license is limited to personal use only in the free form text box at the bottom of the page.

#### GitLab.com
   1. Request that the GitLab Hero sign-up for a free trial on GitLab.com via our [trial page](/free-trial/).
   1. [Create an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) under the [dotcom-internal project](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal) and choose the `plan-change-request` template.
   1. Assign to the Director, Community Relations for approval. Include a brief note about the motivation for this change. 
   1. Once approved by Director, Community Relations, a member of the Support Engineering team will pick up the issue and complete the request. 
   1. Fill in the relevant information including an end date six months after the start date, that plan should be upgraded to Gold, and an end date action of Downgrade to free.

### GitLab Heroes Swag

GitLab Heroes swag is available only for GitLab Heroes. In the near future, a special URL will be sent to Heroes upon acceptance to the program so that they can order swag appropriate for their level in the program. Gitlab Heroes swag production and fulfillment is managed by [Nadel](https://www.nadel.com/). The Nadel team can assist with swag replenishment, ask questions about fulfillment, and other swag-related inquiries. 

### GitLab Heroes Project

The [GitLab Heroes project](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes) is used for discussion via Issues, as a repo for materials that GitLab Heroes create, and a home for other resources for members of the program. Everyone in the program will have Developer access. GitLab Heroes and our community are encouraged to contribute to the project. 

## Community events

We'd love to support you if you are organizing or speaking at a community-driven event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup). Depending on the number and type of attendees at an event, it may be owned by [Corporate Marketing](/handbook/marketing/#new-ideas-for-marketing-campaigns-or-events), [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/#evaluating-potential-field-initiatives), or Community Relations. Our events decision tree is a guide to help you find the right team to handle an event request.

Events Decision Tree:
![event decision tree](/images/handbook/marketing/event-decision-tree.png)

### Submit an event request to our team

To submit a community event for support or sponsorship:

1. Review our events decision tree to ensure you are directing your event inquiry to the appropriate team.
1. Submit an issue using the [sponsorship-request](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request) template.
1. For Service Desk or other auto-generated issues that contain sponsorship requests, we will retroactively apply the 'sponsorship-request' to the issue. The process for updating an issue with no template to the 'sponsorship-request' template is: copy text from original issue, assign 'sponsorship-request' template to issue, paste text from original issue into the appropriate field at bottom of template, update remaining fields.
1. GitLab XDRs: for *contact requests* received in Salesforce via the [Sales webform](/sales/) requesting event sponsorship, please change ownership to `GitLab Evangelist` in SFDC & be sure to "check the box" to send a notification.
1. GitLab's Evangelist Program Manager will review the request and follow up with the event contact.

### How we assess requests

We ask the following questions when assessing an event:
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team member or a member of the wider GitLab community.
- What type of people will be attending the event? We prioritize events attended by diverse groups of developers with an interest in DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We prioritize events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of developers.
- Is the event important for industry and/or open source visibility? We prioritize events that influence trends and attract leaders within the developer and open source communities. We also prioritize events organized by our strategic partners.
- What is the size of the opportunity for the event? We prioritize events based on audience size, the number of interactions we have with attendees, and potential for future contributions to GitLab from attendees.

Each question is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 5 are not eligible for sponsorship.
- Events scoring 6-8 are eligible for GitLab swag for attendees and other in-kind sponsorship.
- Events scoring 9 or above are eligible for financial support.

We ask these questions and use this scorecard to ensure that we're prioritizing GitLab's and our community's best interests when we sponsor events.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

#### Student-run hackathons

Student hackathons are the events most frequently submitting requests for support from GitLab. Hackathon organizers who wish to leverage GitLab's DevOps platform for their events are encouraged to use free trials for their events. This can de done by directing hackathon participants to apply for a [free trial of GitLab](/free-trial/) for use during the hackathon which will allow them to use all of GitLab's features. In some cases, if your event meets [the criteria](/handbook/marketing/community-relations/evangelist-program/#how-we-assess-requests) above at a score of 9 or higher, we may also send stickers for participants or swags as prizes. Given the volume of requests we receive, providing financial support for these events is not feasible.  

## Find a tech speaker

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.). There are a few ways you can get in touch with speakers from the GitLab team and the wider community to participate and do a talk at your event:
* Review our list of [GitLab Heroes members](/community/heroes/members/) to see if there is a GitLab Hero near you. If you'd like to request support from a GitLab Hero, please follow the process detailed in the GitLab Heroes project to [request support from Heroes](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes#requesting-support-from-heroes). 
* Visit our list of active speakers on our [Find a GitLab speaker](/events/find-a-speaker/) page. Once you find a speaker in your region, contact them directly.
* If you are unable to find a speaker in your region, you can complete our [speaker request form](https://docs.google.com/forms/d/e/1FAIpQLSc6jQWbh-63myQu7EBuZZ0KY2J_EKSAZPH6OP2TURNBmfMjtg/viewform). 

For GitLab team members, you can check the #cfp channel on Slack where many of our active tech speakers will see your speaker request. Most speakers will also be able to do talks remotely if the event is virtual or if travel is a challenge.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

## Become a tech speaker

If you are aware of people from the GitLab community who are interested in giving a tech talk relating to GitLab, please direct them to our [Become a Speaker](/community/evangelists/become-a-speaker/) page for more information on the type of support we provide.

For GitLab team members who want to become a tech speaker, contact `evangelists@gitlab.com` and check out the #cfp channel on Slack to discover upcoming opportunities. Additional detail on the logistics of giving a talk once your proposal has been accepted can be found on the [Corporate Marketing](/handbook/marketing/corporate-marketing/#speakers) page.

### Resources for speakers

- [Presentation template](/handbook/tools-and-tips/#google-slides-templates) - our current presentation template.
- [Customer facing presentations](/handbook/marketing/product-marketing/#customer-facing-presentations) - the latest ready-to-present decks from [Strategic Marketing](/handbook/marketing/product-marketing/).
- Additional GitLab-created presentations can be found within the folders contained in [Decks](https://drive.google.com/open?id=0Bz6KrzE1R_3hWGtTRElUWnBzeU0) and [Product Marketing](https://drive.google.com/open?id=0Bz6KrzE1R_3hNjJMNUt2LUJGREU) on the Marketing Drive.
- You can find available speaking opportunities by checking the [#cfp](https://gitlab.slack.com/messages/C106ACT6C) channel on Slack and the issue list for the [Speaker Needed](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues?label_name%5B%5D=Speaker+Needed) label.

## Contribute content

GitLab actively supports content contributors. Our community team tracks GitLab content and our evangelist program manager and editorial team regularly reviews the content.  If you would like to submit your content for review, please create an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=community-content) on our evangelist program project.

We make an effort to amplify and support content contributions that generate value for our community. Criteria we consider include: how well a post addresses an issue in the Community Writers issue tracker, how well a post aligns with our strategy and values, and how well a post is written.

As we identify posts that meet our criteria, we decide how we want to support the posts. We may also identify creators who we want to partner with on content. Writers who are looking for inspiration may want to visit our [Community Writers issue tracker](https://gitlab.com/gitlab-com/community-writers/issues) which tracks blog post ideas submitted by our community.

You can add content you find and track the status of submissions on the Quarterly Community Content issues ([Q2 Issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/128)). Submissions go through the following steps:

1. Review: review submissions for accuracy.
1. Say thanks: contact the creators to say thank you, share a swag code, and highlight other ways they can contribute. $25/creator is the standard amount.
1. Share: Post on social (with a credit to the author) or retweet.
1. Syndicate: for posts that we love or that answer a common or important question, we may ask the author if we can add the post to our Medium publication or the GitLab blog.
1. Curate: creators who have shown a depth of experience around topics important to GitLab and the wider GitLab community may be asked to submit talks or posts about said topics.

### Blog

Coming soon. Contact `evangelists@gitlab.com` if you have any questions.

### Videos

Coming soon. Contact `evangelists@gitlab.com` if you have any questions.

## Evangelist Program Office Hours

Our Evangelist Program Manager hosts office hours via Zoom every Friday at 10:30am ET excluding holidays. They want to answer your meetup, events, and public speaking questions and hear your feedback on our programs! You can see the meeting information and join the call via the [Community Events](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) calendar.

## Helpful Resources

- [Community Events Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York)
- [Meetups Checklist](/community/meetups/checklist/)
- [Merchandise](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
- [Find a speaker](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html)
