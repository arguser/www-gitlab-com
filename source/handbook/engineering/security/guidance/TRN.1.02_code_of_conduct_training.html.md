---
layout: handbook-page-toc
title: "TRN.1.02 - Code of Conduct Training Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TRN.1.02 - Code of Conduct Training

## Control Statement

All GitLab team-members complete a code of business conduct training.

## Context

The aim of this control is help ensure that all GitLab team-members are aligned on the values of the organization. The purpose of this alignment is to demonstrate to any external auditors that we hold all GitLab team-members to this same standard of conduct.

## Scope

This control applies to all GitLab team-members and contractors.

## Ownership

Control owner: 
* `People Operations`

Process owner: 
* People Operations

## Guidance

People Ops are responsible for deploying the process to ensure 100% of employee training and validating that every GitLab team member has provided their signed acknowledgement of the code of conduct in the current year. All GitLab team members are responsible for competing the training of the GitLab Business Ethics and Code of Conduct.

## Additional control information and project tracking

The security training is delivered by prompting team members to review the GitLab Business Ethics and Code of Conduct and upload their signed acknowledgment upon completion. The training is linked from onboarding issue template as part of the new hire tasks. The [2020 Code of Conduct Training](https://gitlab.com/gitlab-com/people-group/General/issues/591#problem-or-issue-statement) is in the planning stage to be rolled out and completed by the end of March 2020. Legal and Security is consulted for the content. 

For audit evidence of compliance, we need to be able to demonstrate 100% completion of training by all team members.

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Code of Conduct Training control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/932).

### Policy Reference

* Handbook policy [GitLab Business Ethics and Code of Conduct](https://about.gitlab.com/handbook/people-group/code-of-conduct/)
* [Acknowledgement form template](https://about.gitlab.com/handbook/people-group/code-of-conduct/#code-of-business-conduct--ethics-acknowledgment-form)

## Framework Mapping

* ISO
  * A.11.2.8
  * A.7.1.2
  * A.7.2.1
  * A.8.1.3
* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
* PCI
  * 12.3
  * 12.3.5
