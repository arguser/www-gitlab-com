---
layout: handbook-page-toc
title: "Canada Corp"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Canadian Team Members

Benefits are administered by Great West Life. The benefits decision discussions are held by Total Rewards and the Comp Group to elect the next year's benefits by the carrier's deadlines. Total Rewards will notify the team of open enrollment as soon as details become available.

GitLab covers **100%** of team member, spouse, dependents, and/or domestic partner premiums for medical, dental, and vision coverage.

## Group Health Coverage

GitLab offers medical, dental, and vision coverage at no cost to the team member. 

### Medical Coverages

**Coverage Summary:**
* Prescription Drugs: 80% - Nil deductible; 90% if purchased at Costco
* Pay Direct Drug Card: Included - Generic Mandatory
* Maximum:	Unlimited
* Fertility Drugs: Unlimited as per all prescription medication
* Smoking Cessations: $500 lifetime maximum
* Vaccines: Included - reasonable & customary maximum
* Major Medical: 80%
* Annual Deductible: None
* Hospitalization: 100% - Bill deductible - Semi-Private
* Orthotic Shoes: $ 300 per benefit year maximum
* Othotic Inserts: combined with shoes
* Hearing Aids: $700 every 5 years
* Paramedical Practitioners: 80% - $300 per practitioner per benefit year maximum
* Included Specialists: Chiropractor, Osteopath, Naturopath, Podiatrist, Chirpodist, Speech Therapist, Psychologist, Social Worker, Massage Therapist, Physiotherapist, Acupuncturist, Dietician, No Doctor's referral required on any paramedical services
* Out of Country: 100% - Nill deductible - Unlimited
* Maximum Duration: 60 consecutive days
* Trip Cancellation: Not Included
* Private Duty Nursing: $5,000 for a maximum of 12 months per condition
* Survivor Benefit: 24 months
* Termination Age: Retirement

### Dental Coverages

**Coverage Summary:**
* Annual Deductible: None
* Basic & Preventative: 80%
* Periodontic & Endodontic:	80%
  * Annual Maximum: $2,000 per benefit year
* Major Restorative Services: 50%
  * Annual Maximum: $2,000 per benefit year
* Orthodontic Services: 50% (children age 6 and older)
  * Lifetime Maximum: $1,500
* Recall visit: Two visits per benefit year
* Scaling & Rooting Units: 10 in any 12 months
* White Filings: Included
* Free Guide: Current
* Survivor Benefit: 24 months
* Termation Age: Retirement

### Vision Coverages

**Coverage Summary:**
* Vision Care: 100% - $200 every 24 months
* Eye Exams: 100% - 1 visit every 24 months - reasonable & customary maximum

## Life Insurance / AD&D

**Coverage Summary:**
* Benefit: 2 times annual earnings to a maximum of $250,000
* Reduction: Reduces by 50% at age 65
* Termination Age: Reduces by 50% at age 65

## Long Term Disability

**Coverage Summary:**
* Benefit: 66.67% of monthly earnings to a maximum of $12,500 (Taxable)
* Monthly Maximum: 12,500
* Tax Status: Taxable
* Elimination Period: 90 days
* Benefit Period: To age 65
* Definition: 2 years own occupation
* Offsets: Primary
* Cost of Living Adjustment: Not included
* Non-Evidence Maximum: 5,600
* Termination: Age 65

## Vacation Pay

* Individuals will receive 4-6% vacation pay depending on the province where applicable. Vacation pay is 4-6% of all gross earnings.

### Canada Pension Plan (CPP)

* Employer contributions are made to the federal Canada Pension Plan (CPP).

### Enrollment  

GitLab has chosen a digital administration platform to manage our benefits administration. Consider this platform as your single source of truth when it comes to benefits, where you can enroll in the plan, update your enrollment information, and view your coverage that will be in effect March 1, 2020.

**WHAT YOU NEED TO DO:**
You will be receiving an invitation from `no-reply@collage.co` with a personalized link. Click on that link to set up your profile. Please reach out to the Compensation & Benefits team at `compensation@gitlab.com` if you have any questions! Here are some additional [instructions](https://drive.google.com/file/d/0B4eFM43gu7VPRWZFOHBxMUZnaUVoOGxWbzVSNEdiWk9qOVpR/view?usp=sharing) on how to set up your account and enroll in benefits.

#### Enrollment of Domestic Partners 

Great West Life uses the first day cohabitating as the date of eligibility to enter into the GitLab health plan. Anyone who has a common law spouse that they wish to add, moving from Single to Family coverage, for example, will need to do so within 30 days of moving in together. 

 
