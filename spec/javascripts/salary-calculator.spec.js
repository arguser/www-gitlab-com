/* eslint-env jasmine */
/* eslint-disable global-require */
/* global Promise */
const { mount } = require('@vue/test-utils');
const component = require('../../source/frontend/components/salary-calculator/SalaryCalculator.vue').default;

describe('the salary calculator', function () {
  var SalaryCalculator;

  var mockData = {
    contractTypes: [{
      country: 'United States',
      employee_factor: 1,
      entity: 'GitLab Inc'
    }, {
      country: '*',
      contractor_factor: 1.17,
      entity: 'GitLab BV'
    }],
    countryNoHire: [
      'Brazil'
    ],
    currencyExchangeRates: {
      rates_to_usd: [{
        currency_code: 'CAD',
        rate: 0.76088,
        countries: ['Canada']
      }]
    },
    locationFactors: [{
      country: 'United States',
      area: 'Columbus, Ohio',
      locationFactor: 47.36
    }, {
      country: 'Canada',
      area: 'Alberta',
      locationFactor: 36.5
    }],
    roles: [{
      title: 'Backend Engineer',
      levels: 'engineering_ic',
      ic_ttc: {
        compensation: 160000,
        percentage_variable: 0,
        from_base: true
      }
    }, {
      title: 'Engineering Management, Quality',
      levels: false,
      ic_ttc: {
        compensation: 182563,
        percentage_variable: 0,
        from_base: true
      }
    }],
    roleLevels: {
      engineering_ic: [{
        title: 'Junior',
        factor: 0.8,
        type: 'ic_ttc'
      }, {
        title: 'Intermediate',
        factor: 1.0,
        is_default: true,
        type: 'ic_ttc'
      }]
    }
  };


  jest.mock('jquery', () => {
    return {
      get: () => jest.fn()
    }
  });

  beforeEach(function () {
    SalaryCalculator = mount(component);
    SalaryCalculator.vm.setData(mockData);
  });

  describe('methods', function () {
    describe('setRoleLevels', function () {
      it('sets the default level if none are available', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[1] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentLevel).toEqual({ title: 'N/A', factor: 1, type: 'ic_ttc' });
      });

      it('sets the current level from the role, if available', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setRoleLevels();

        expect(SalaryCalculator.vm.currentLevel).toEqual(mockData.roleLevels.engineering_ic[1]);
      });
    });

    describe('setParamsFromUrl', function () {
      it('sets values from the URL', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0] });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?country=United+States&area=Columbus,+Ohio&level=Junior&comparatio=Learning+the+role');

        expect(SalaryCalculator.vm.currentCountry).toEqual('United States');
        expect(SalaryCalculator.vm.currentArea.area).toEqual('Columbus, Ohio');
        expect(SalaryCalculator.vm.currentLevel.title).toEqual('Junior');
        expect(SalaryCalculator.vm.currentCompaRatio.label).toEqual('Learning the role');
      });

      it('skips params not in the URL', function () {
        SalaryCalculator.setData({ currentRole: mockData.roles[0], currentCountry: null });
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?level=Intermediate');

        expect(SalaryCalculator.vm.currentCountry).toBe(null);
        expect(SalaryCalculator.vm.currentLevel.title).toEqual('Intermediate');
      });

      it('only sets areas valid for the country', function () {
        SalaryCalculator.vm.setParamsFromUrl('http://example.com/?country=Canada&area=Columbus,+Ohio');

        expect(SalaryCalculator.vm.currentCountry).toEqual('Canada');
        expect(SalaryCalculator.vm.currentArea).toBe(null);
      });
    });

    describe('formatAmount', function () {
      it('formats the amount in USD by default', function () {
        expect(SalaryCalculator.vm.formatAmount(1234)).toEqual('$1,234');
      });

      it('formats the amount in another currency code when passed', function () {
        expect(SalaryCalculator.vm.formatAmount(1234, 'AUD')).toEqual('1,234 AUD');
      });
    });

    describe('formatAreaLocationFactor', function () {
      it('formats the number to three decimal places', function () {
        expect(SalaryCalculator.vm.formatAreaLocationFactor({ locationFactor: 0.12345 })).toEqual('0.123');
      });

      it('returns "--" when null is passed', function () {
        expect(SalaryCalculator.vm.formatAreaLocationFactor()).toEqual('--');
      });
    });

    describe('findByCountry', function () {
      it('finds a result matching the country name', function () {
        expect(SalaryCalculator.vm.findByCountry(mockData.contractTypes, 'United States')).toEqual(mockData.contractTypes[0]);
      });

      it('falls back to * when there is no match', function () {
        expect(SalaryCalculator.vm.findByCountry(mockData.contractTypes, 'Somewhere else')).toEqual(mockData.contractTypes[1]);
      });
    });
  });
});
